<?php

class Helpers {

  public static function rupiah($int) {
    return 'Rp '.number_format($int, 2, ',', '.');
  }

  public static function idr($int) {
    return number_format($int, 2, ',', '.').' IDR';
  }

}
