<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('login/facebook', function() {
    // get data from input
    $code = Input::get( 'code' );

    // get fb service
    $fb = OAuth::consumer( 'Facebook' );

    // check if code is valid

    // if code is provided get user data and sign in
    if ( !empty( $code ) ) {

        // This was a callback request from facebook, get the token
        $token = $fb->requestAccessToken( $code );

        // Send a request with it
        $result = json_decode( $fb->request( '/me?fields=id,name,email,picture' ), true );

        // $message = 'Your unique facebook user id is: ' . $result['id'] . ' and your name is ' . $result['name'];
        //  echo $message. "<br/>";

        //Var_dump
        //display whole array().
        // dd($result);
        $customer = Customer::where('facebook_id', $result['id'])->first();
        if ($customer) {
          Auth::login($customer);
          return 'telah ada';
        } else {
          $newCustomer = new Customer;
          $newCustomer->facebook_id = $result['id'];
          $newCustomer->name = $result['name'];
          $newCustomer->email = $result['email'];
          $newCustomer->url = $result['picture']['data']['url'];
          $newCustomer->save();
          Auth::login($newCustomer);
          return 'Welcome';
        }

    }
    // if not ask for permission first
    else {
        // get fb authorization
        $url = $fb->getAuthorizationUri();

        // return to facebook login url
        return Redirect::to( (string)$url );
    }

});
// Redirect to Facebook for authorization

Route::controller('mindo', 'MindoController');

Route::get('/', 'HomeController@getIndex');
Route::controller('file', 'FileController');
Route::controller('auth', 'AuthController');
Route::controller('product', 'ProductController');
Route::controller('rajaongkir', 'RajaOngkirController');
Route::group(['before' => 'customer'], function()
{
  Route::controller('order', 'OrderController');
  Route::controller('user', 'UserController');
});

Route::group(['prefix' => 'cm-admin'], function()
{
  Route::controller('auth', 'AdminAuthController');
  Route::group(['before' => 'administrator'], function()
  {
    Route::get('/', 'AdminHomeController@getIndex');
    Route::controller('product', 'AdminProductController');
    Route::controller('order', 'AdminOrderController');
    Route::controller('vector', 'AdminVectorController');


  });

});
