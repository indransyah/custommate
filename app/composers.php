<?php

View::composer(['frontend.layouts.header', 'frontend.products.show'], function($view)
{
  $check = Sentry::check();
  if (!$check) {
    $status = false;
    $customer = null;
  } else {
    $status = true;
    $customer = Sentry::getUser();
    if (!$customer->hasAccess('customer') || $customer == null) {
      $status = false;
    }
  }
  $view->with(array('status' => $status, 'customer' => $customer));
});

View::composer(['backend.layouts.header'], function($view)
{
  $administrator = Sentry::getUser();
  $view->with(['administrator' => $administrator]);
});

// View::creator(['frontend.layouts.header'], function($view) {
//   $check = Sentry::check();
//   if (!$check) {
//     $status = false;
//     $customer = null;
//   } else {
//     $status = true;
//     $customer = Sentry::getUser();
//     if (!$customer->hasAccess('customer') || $customer == null) {
//       $status = false;
//     }
//   }
//   $view->with(array('status' => $status, 'customer' => $customer));
// });

// View::creator(['backend.layouts.header'], function($view)
// {
//   $administrator = Sentry::getUser();
//   $view->with(['administrator' => $administrator]);
// });
