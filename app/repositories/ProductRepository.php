<?php

class ProductRepository implements ProductInterface {

  /**
   * Variable untuk menyimpan error pada validasi
   */
  public $errors;

  /**
   * Menambah produk
   */
  public function store($input)
  {
    // Inisialisasi
    $product = new Product;
    $product->name = $input['name'];
    $product->slug = Str::slug($input['name'], '-');
    $product->sku = $this->generateSku();
    $product->price = $input['price'];
    $product->weight = $input['weight'];
    $product->description = $input['description'];
    $product->status = 'published';
    $product->front_photo = $input['front_photo'];
    $product->back_photo = $input['back_photo'];

    // Cek validasi karena memakai Stapler jadi tidak bisa langsung disimpan
    $validation = $product->validate();
    if(!$validation)
    {
      $this->errors = $product->errors();
      return false;
    }

    // Hapus rules validasi agar dapat disimpan menggunakan Stapler
    unset($product::$rules['front_photo']);
    unset($product::$rules['back_photo']);

    // Inisialisasi Stapler
    $product->front = $input['front_photo'];
    $product->back = $input['back_photo'];

    // Simpan
    $storage = $product->save();
    if (!$storage)
    {
      $this->errors = $product->errors();
      return false;
    }
    return array('status' => true, 'id' => $product->id);
  }

  /**
   * Generate random SKU
   */
  public function generateSku()
  {
    do {
      $random = strtoupper(str_random(8));
    } while (Product::where('sku', $random)->exists());
    return $random;
  }

  public function all() {
    return Product::all(['id', 'sku', 'name', 'price', 'slug', 'status']);

  }

  public function find($id) {
    return Product::findOrFail($id);
  }

  /**
   * Fungsi untuk memecah id yang dikirim lewat ajax
   */
  public function explode($tableData)
  {
    if (is_null($tableData)) {
        return false;
    }
    $ids = explode(',', $tableData);
    if (!is_array($ids)) {
        return false;
    }
    return $ids;
  }

  /**
   * Menghandle bulk action dari product index
   */
  public function bulkAction($tableData, $status)
  {
    $ids = $this->explode($tableData);
    if (!$ids) {
        return false;
    }
    $update = Product::whereIn('id', $ids)->update(array('status' => $status));
    if (!$update) {
        return false;
    }
    return true;
  }

  /**
   * Menghandle bulk delete dari product index
   */
  public function bulkDelete($tableData)
  {
    $ids = $this->explode($tableData);
    if (!$ids) {
        return false;
    }
    $delete = Product::whereIn('id', $ids)->delete();
    if (!$delete) {
        return false;
    }
    return true;
  }

  /**
   * Update product
   */
  public function update($id, $input) {
    $product = Product::findOrFail($id);
    $product->name = $input['name'];
    $product->slug = Str::slug($input['name'], '-');
    $product->sku = $this->generateSku();
    $product->price = $input['price'];
    $product->weight = $input['weight'];
    $product->description = $input['description'];
    $product->front_photo = $input['front_photo'];
    $product->back_photo = $input['back_photo'];
    $rules = $product::$rules;
    $rules['front_photo'] = 'mimes:png';
    $rules['back_photo'] = 'mimes:png';
    $validation = $product->validateUniques($rules);
    if(!$validation)
    {
      $this->errors = $product->errors();
      return false;
    }
    unset($rules['front_photo']);
    unset($rules['back_photo']);
    $product->front = $input['front_photo'];
    $product->back = $input['back_photo'];
    $update = $product->updateUniques($rules);
    if (!$update) {
      $this->errors = $product->errors();
      return false;
    }
    return true;
  }

  /**
   * Menambah warna product
   */
  public function addColor($id)
  {
    $color = ltrim(Input::get('color'), '#');
    if (ctype_xdigit($color) == false && (strlen($color) != 6 || strlen($color) != 3))
      return array('status' => false, 'message' => 'invalid');

    $product = Product::findOrFail($id);
    $rules = $product::$rules;
    unset($rules['front_photo']);
    unset($rules['back_photo']);
    $temp = array();
    if($product->color != null) {
      $temp = json_decode($product->color);
    }
    foreach($temp as $value) {
      if($value->color == Input::get('color'))
        return array('status' => false, 'message' => 'exist');
    }
    $temp[] = array('color' => Input::get('color'));
    $product->color = json_encode($temp);
    $product->updateUniques($rules);
    return array('status' => true);
  }

  /**
   * Menghapus warna produk
   */
  public function deleteColor($id)
  {
    $product = Product::findOrFail($id);
    $rules = $product::$rules;
    unset($rules['front_photo']);
    unset($rules['back_photo']);
    $color = Input::get('color');
    $colors = json_decode($product->color);
    foreach($colors as $key => $value) {
      if($color == $value->color)
        unset($colors[$key]);
    }
    $product->color = json_encode(array_values($colors));
    $status = $product->updateUniques($rules);
    if($status)
      return true;
    return false;
  }

  // FRONTEND
  public function allFields()
	{
		return Product::all();
	}

	public function get($slug)
	{
		$product = Product::where('slug', $slug)->first();
		if (!$product) {
			return false;
		}
		return $product;
	}

}
