<?php

class RajaOngkirRepository implements RajaOngkirInterface {

  protected $api;

  public function __construct()
  {
      $this->api = '440f0e83184ab706a18f9daa0392e0bb';
  }

  public function province($province_id = null)
  {
    if (is_null($province_id)) {
      $url = 'http://rajaongkir.com/api/starter/province';
    } else {
      $url = 'http://rajaongkir.com/api/starter/province/?id='.$province_id;
    }
    $client = new GuzzleHttp\Client();
    $res = $client->request('GET', $url, [
      'headers' => [
        'key' => $this->api
      ]
    ]);
    $response = $res->getBody();
    $response = json_decode($response);
    return $response->rajaongkir->results;
  }

  public function city($province_id = null, $city_id = null)
  {
    if (is_null($province_id) && is_null($city_id)) {
      $url = 'http://rajaongkir.com/api/starter/city';
    } elseif(is_null($province_id)) {
      $url = 'http://rajaongkir.com/api/starter/city/?id='.$city_id;
    } elseif (is_null($city_id)) {
      $url = 'http://rajaongkir.com/api/starter/city/?province='.$province_id;
    } else {
      $url = 'http://rajaongkir.com/api/starter/city/?province='.$province_id.'&id='.$city_id;
    }
    $client = new GuzzleHttp\Client();
    $res = $client->request('GET', $url, [
      'headers' => [
        'key' => $this->api
      ]
    ]);
    $response = $res->getBody();
    $response = json_decode($response);
    return $response->rajaongkir->results;
  }

  public function cost($input)
  {
    $url = 'http://rajaongkir.com/api/starter/cost';
    $client = new GuzzleHttp\Client();
    $res = $client->request('POST', $url, [
      'headers' => [
        'key' => $this->api
      ],
      'form_params' => [
        'origin'     => 501,
        'destination' => $input['destination'],
        'weight' => $input['weight'],
        'courier' => $input['courier'],
      ]
      // 'multipart' => [
      //   'origin'     => 501,
      //   'destination' => 114,
      //   'weight' => 1000,
      //   'courier' => 'jne',
      //   ]
    ]);
    $response = $res->getBody();
    $response = json_decode($response);
    return $response->rajaongkir->results;
  }

  public function detail($input, $weight)
  {
    $url = 'http://rajaongkir.com/api/starter/cost';
    $client = new GuzzleHttp\Client();
    $res = $client->request('POST', $url, [
      'headers' => [
        'key' => $this->api
      ],
      'form_params' => [
        'origin'     => 501,
        'destination' => $input['kabupaten'],
        'weight' => $weight,
        'courier' => $input['kurir'],
      ]
    ]);
    $response = $res->getBody();
    $response = json_decode($response);
    return $response->rajaongkir;
  }

}
