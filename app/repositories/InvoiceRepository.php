<?php

class InvoiceRepository implements InvoiceInterface {

  public function lists()
  {
    return Invoice::whereHas('order', function($q) {
      $q->whereStatus('pembayaran');
      })->lists('code', 'id');
  }

  public function getByCode($code)
  {
    return Invoice::whereCode($code)->first();
  }

}
