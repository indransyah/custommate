<?php

class UserRepository implements UserInterface {

  public function order($user_id)
  {
    return Order::where('user_id', $user_id)->get();
  }

  public function orderDetail($id)
  {
    return Order::findOrFail($id);
  }

}
