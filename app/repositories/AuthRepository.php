<?php

class AuthRepository implements AuthInterface {

  public function facebook($result) {
    try
    {
      // Create the user
      $newCustomer = Sentry::createUser(array(
        'facebook_id' => $result['id'],
        'email'       => $result['email'],
        'name'        => $result['name'],
        'avatar'      => $result['picture']['data']['url'],
        'password'    => str_random(32),
        'activated'   => true,
      ));

      // Find the group using the group id
      $customerGroup = Sentry::findGroupById(2);

      // Assign the group to the user
      $newCustomer->addGroup($customerGroup);
      Sentry::login($newCustomer, true);
      return array('status' => 1);
    }
    catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
    {
      $message = 'Login field is required.';
      return array('status' => 0, 'message' => $message);
    }
    catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
    {
      $message = 'Password field is required.';
      return array('status' => 0, 'message' => $message);
    }
    catch (Cartalyst\Sentry\Users\UserExistsException $e)
    {
      $message = 'User with this login already exists.';
      return array('status' => 0, 'message' => $message);
    }
    catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
    {
      $message = 'Group was not found.';
      return array('status' => 0, 'message' => $message);
    }
  }

  public function addUser($input) {
    try
    {
      // Create the user
      $newCustomer = Sentry::createUser(array(
        'facebook_id' => $input['id'],
        'email'       => $input['email'],
        'name'        => $input['name'],
        'password'    => str_random(32),
        'activated'   => true,
      ));

      // Find the group using the group id
      $customerGroup = Sentry::findGroupById(2);

      // Assign the group to the user
      $newCustomer->addGroup($customerGroup);
      Sentry::login($newCustomer, true);
      return true;
    }
    catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
    {
      return false;
    }
    catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
    {
      return false;
    }
    catch (Cartalyst\Sentry\Users\UserExistsException $e)
    {
      return false;
    }
    catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
    {
      return false;
    }
  }
}
