<?php

class FileRepository implements FileInterface
{

	/**
	 * Memproses upload image
	 */
	public function uploadImage($image, $name)
	{
		$upload = $image->move('images/', $name);
		if ($upload)
			return true;
		return false;
	}

	/**
	 * Validasi image yang diupload
	 * @return false (jika file valid) | (jika file valid) pesan error validasi
	 */
	public function invalidImage($input)
	{
		$rules = array(
			'image' => 'required|image|mimes:png'
		);
		$validation = Validator::make($input, $rules);
		if (!$validation->fails())
			return false;
		return $validation->messages();
	}

	/**
	 * Memproses upload design
	 */
	public function uploadDesign($input, $slug)
	{
		// Cek terdapat inputan atau tidak
		if (!isset($input['front']) || !isset($input['back'])) {
			return false;
		}

		// Cek kosong atau tidak
		if (is_null($input['front']) || is_null($input['back'])) {
			return false;
		}

		// Ubah data base64 menjadi file & menyimpannya
		$design = array();
		$design['product'] = $slug;
		foreach ($input as $key => $value) {
			$name = $this->base64ToFile($value);
			if (!$name) {
				return false;
			}
			$design[$key] = $name;
		}
		Session::put('design', $design);
		return true;

	}

	/**
	 * Menyimpan design file (base64)
	 * @return false (jika gagal menyimpan) | nama file (jika berhasil disimpan)
	 */
	public function base64ToFile($base64)
	{
		$name = uniqid();
		$base64 = str_replace('data:image/png;base64,', '', $base64);
		$base64 = base64_decode($base64);
		$file = 'designs/temp/'. $name . '.png';
		$status = File::put($file, $base64);
		if (!$status) {
			return false;
		}
		return $name.'.png';
	}

	/**
	* Memindah design file jika berhasil diorder
	*/
	public function moveDesign($file)
	{
		rename('designs/temp/'.$file['front'], 'designs/'.$file['front']);
		rename('designs/temp/'.$file['back'], 'designs/'.$file['back']);
	}

}
