<?php

class VectorRepository implements VectorInterface {

  protected $rules = array(
    'vector' => 'required|mimes:zip'
  );

  public $errors;

  public function validation($input)
  {
    $vector = Vector::findOrFail($input['id']);
    $vector->status = $input['status'];
    $vector->message = $input['message'];
    $store = $vector->save();
    return $store;
  }

  public function delete($id)
  {
    $vector = Vector::findOrFail($id);
    $delete = $vector->delete();
    if ($delete === false)
    {
      return false;
    }
    return true;
  }

  // FRONTEND
  public function validator($input)
  {
    $validator = Validator::make($input, $this->rules);
    if ($validator->fails())
    {
      $this->errors = $validator->messages();
      return false;
    }
    return true;
  }

  public function upload($design_id, $input)
  {
    $validation = $this->validator($input);
    if ($validation === false) {
      return false;
    }

    $path = Vector::$path;
    $name = $input['vector']->getClientOriginalName();
    $upload = $input['vector']->move($path, $name);
    if ($upload === false) {
      return false;
    }
    $vector = new Vector;
    $vector->name = $name;
    $vector->path = $path;
    $vector->design_id = $design_id;
    $store = $vector->save();
    if ($store === false) {
      return false;
    }
    return true;
  }
  
}
