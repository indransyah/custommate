<?php

class PaymentRepository implements PaymentInterface {

  public function add($input)
  {
    $payment = new Payment;
    $payment->destination = $input['bank-tujuan'];
    $payment->origin = $input['bank-asal'];
    $payment->total = $input['jumlah'];
    $payment->name = $input['nama'];
    $payment->message = $input['pesan'];
    $payment->date = $input['tanggal'];
    $payment->invoice_id = $input['invoice'];
    $payment->save();
  }

}
