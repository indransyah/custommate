<?php

class OrderRepository implements OrderInterface {

  protected $rules = array(
    'nama' => 'required',
    'telepon' => 'required|numeric',
    'provinsi' => 'required',
    'kabupaten' => 'required',
    'alamat' => 'required',
    'kode_pos' => 'required|numeric',
    'kurir' => 'required',
    'servis' => 'required',
    'desain' => 'required|mimes:zip',
  );
  
  public $errors;

  public function all()
  {
    return Order::with(array('user' => function($query) {
      $query->select(['id', 'name']);
    }, 'invoice'))->get();
  }

  public function get($id)
  {
    return Order::findOrFail($id);
  }

  public function status($id, $status)
  {
    $availableStatus = array('upload', 'validasi', 'pembayaran', 'konfirmasi', 'produksi', 'pengiriman', 'selesai');
    if (!in_array($status, $availableStatus)) {
      return false;
    }
    $order = $this->get($id);
    $order->status = $status;
    $store = $order->save();
    if ($store === false) {
      return false;
    }
    return true;
  }

  // FRONTEND
  public function add($input, $product)
  {
    // Tidak ada respon dari Cart::add()
    Cart::associate('Product')->add($product->id, $product->name, $input['jumlah'], $product->price, array('size' => $input['ukuran']));
  }

  public function allFields()
  {
    return Cart::content();
  }

  public function weight($product)
  {
    return $product->weight * Cart::count();
  }

  public function validator($input)
  {
    $validator = Validator::make($input, $this->rules);
    if ($validator->fails())
    {
      $this->errors = $validator->messages();
      return false;
    }
    return true;
  }

  public function order($customer_id, $product_name, $cart, $total)
  {
    $order = new Order;
    $order->total = $total;
    $order->product_name = $product_name;
    $order->status = 'verifikasi';
    $order->user_id = $customer_id;
    $store = $order->save();
    if (!$store) {
      return false;
    }
    foreach ($cart as $key => $value) {
      $attach = $order->product()->attach($value->id, array('qty' => $value->qty, 'size' => $value->options->size, 'price' => $value->price, 'subtotal' => $value->subtotal));
      if ($attach === false) {
        return false;
      }
    }
    return $order;
  }

  public function recipient($customer_id, $input, $province, $city, $postal_code)
  {
    $recipient = new Recipient;
    $recipient->name = $input['nama'];
    $recipient->phone = $input['telepon'];
    $recipient->address = $input['alamat'];
    $recipient->postal_code = $input['kode_pos'];
    $recipient->province = $province;
    $recipient->city = $city;
    $recipient->user_id = $customer_id;
    $store = $recipient->save();
    if ($store === false) {
      return false;
    }
    return $recipient;
  }

  public function shipment($service, $order_id, $recipient_id, $courier, $ongkir, $originCity, $originDestination, $weight)
  {
    $shipment = new Shipment;
    $shipment->courier = $courier;
    $shipment->service = $service;
    $shipment->cost = $ongkir;
    $shipment->weight = $weight;
    $shipment->origin = $originCity;
    $shipment->destination = $originDestination;
    $shipment->status = 'verifikasi';
    $shipment->order_id = $order_id;
    $shipment->recipient_id = $recipient_id;
    $store = $shipment->save();
    if ($store === false) {
      return false;
    }
    return true;
  }

  public function design($file, $order_id)
  {
    $design = new Design;
    $design->front = $file['front'];
    $design->back = $file['back'];
    $design->order_id = $order_id;
    $store = $design->save();
    if ($store === false) {
      return false;
    }
    return $design;
  }

  public function vector($input, $design_id)
  {
    $path = Vector::$path;
    $name = $input['desain']->getClientOriginalName();
    $upload = $input['desain']->move($path, $name);
    if ($upload === false) {
      return false;
    }
    $vector = new Vector;
    $vector->name = $name;
    $vector->path = $path;
    $vector->design_id = $design_id;
    $store = $vector->save();
    if ($store === false) {
      return false;
    }
    return true;

  }

  public function invoice($order_id, $totalPlusOngkir)
  {
    $random = strtoupper(str_random(5));
    $unique = true;
		while ($unique === true) {
			$unique = Invoice::where('code', $random)->exists();
			$random = strtoupper(str_random(5));
		}
    $invoice = new Invoice();
    $invoice->code = $random;
    $invoice->total = $totalPlusOngkir;
    $invoice->status = 'verifikasi';
    $invoice->order_id = $order_id;
    $store = $invoice->save();
    if ($store === false) {
      return false;
    }
    return true;
  }

  public function lists()
  {
    return Order::whereStatus('pembayaran')->lists('order_code', 'id');
  }

}
