<?php

class Payment extends Eloquent {

	/**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'payments';

	public static function boot() {
    parent::boot();
    Payment::creating(function($payment)
		{
			$payment->invoice->order->status = 'konfirmasi';
			$payment->invoice->order->save();
    });
  }

	public function invoice()
	{
		return $this->belongsTo('Invoice');
	}

}
