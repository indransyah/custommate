<?php

use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;
use LaravelBook\Ardent\Ardent;

/**
 *
 */
class Image extends Ardent implements StaplerableInterface {

  use EloquentTrait;

  public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('image');
        parent::__construct($attributes);
    }

  public static $rules = array(
      'image' => 'required|image|mimes:jpeg,png'
    );
}
