<?php

class Invoice extends Eloquent {

	/**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'invoices';

	public function order()
	{
		return $this->belongsTo('Order');
	}

	public function payment() {
		return $this->hasOne('Payment');
	}

}
