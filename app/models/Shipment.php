<?php

class Shipment extends Eloquent {

	/**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'shipments';

	public function order()
	{
		return $this->belongsTo('Order');
	}

	public function recipient()
	{
		return $this->belongsTo('Recipient');
	}
}
