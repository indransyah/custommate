<?php

use LaravelBook\Ardent\Ardent;

/**
 *
 */
class Gambar extends Ardent {

  protected $table = 'images';

  public static $rules = array(
      'image' => 'required|image'
    );
}
