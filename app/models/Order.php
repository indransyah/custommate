<?php

class Order extends Eloquent {

	/**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'orders';

	public function product()
	{
		return $this->belongsToMany('Product', 'order_detail', 'order_id', 'product_id')->withPivot('qty', 'size', 'price', 'subtotal');
	}

	public function shipment() {
		return $this->hasOne('Shipment');
	}

	public function design() {
		return $this->hasOne('Design');
	}

	public function invoice() {
		return $this->hasOne('Invoice');
	}

	public function user() {
		return $this->belongsTo('User');
	}
}
