<?php

use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;
use LaravelBook\Ardent\Ardent;

class Product extends Ardent implements StaplerableInterface {

  use EloquentTrait;

  protected $table = 'products';

  public static $rules = array(
    'name' => 'required|between:4,16|unique:products',
    'price' => 'required|integer',
    'weight' => 'required|integer',
    'front_photo' => 'required|mimes:png',
    'back_photo' => 'required|mimes:png'
  );

  public function __construct(array $attributes = array()) {
    $this->hasAttachedFile('front');
    $this->hasAttachedFile('back');

    parent::__construct($attributes);
  }

  public function order()
  {
    return $this->belongsToMany('Order', 'order_detail', 'product_id', 'order_id')->withPivot('qty', 'size', 'price', 'subtotal');
  }

}
