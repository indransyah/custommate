<?php

class Recipient extends Eloquent {

	/**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'recipients';

	public function user()
	{
		return $this->belongsTo('User');
	}
}
