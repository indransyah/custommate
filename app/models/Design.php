<?php

class Design extends Eloquent {

  protected $table = 'designs';

  public static function boot() {
    parent::boot();
    Design::updating(function($design) {
      if ($design->status == 'uploaded') {
        $orderStatus = 'pembayaran';
      } elseif ($design->status == 'resubmit') {
        $orderStatus = 'upload';
      }
      $design->order->status = $orderStatus;
      $design->order->save();

    });
  }

  public function order()
	{
		return $this->belongsTo('Order');
	}

  public function vector()
	{
		return $this->hasMany('Vector');
	}

}
