<?php

class Vector extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'vectors';

	public static $path = 'vectors/';

   public static function boot() {
     parent::boot();
		 Vector::creating(function($vector) {
			 $vector->design->order->status = 'validasi';
			 $vector->design->order->save();
		 });
		 Vector::updating(function($vector) {
       if ($vector->status == 'valid') {
         $designStatus = 'uploaded';
       } elseif ($vector->status == 'invalid') {
         $designStatus = 'resubmit';
       }
			 $vector->design->status = $designStatus;
       $vector->design->save();
     });
     Vector::deleting(function($file) {
       File::delete($file->path.$file->name);
     });
   }

   public function design()
 	{
 		return $this->belongsTo('Design');
 	}

}
