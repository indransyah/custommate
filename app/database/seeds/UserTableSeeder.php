<?php

class UserTableSeeder extends Seeder {

	public function run()
	{
		try
		{
			// Create the group
			$admin = Sentry::createGroup(array(
				'name'        => 'Administrator',
				'permissions' => array(
					'admin' => 1,
					'customer' => 0,
				),
			));
			$customer = Sentry::createGroup(array(
				'name'        => 'Customer',
				'permissions' => array(
					'admin' => 0,
					'customer' => 1,
				),
			));
		}
		catch (Cartalyst\Sentry\Groups\NameRequiredException $e)
		{
			echo 'Name field is required';
		}
		catch (Cartalyst\Sentry\Groups\GroupExistsException $e)
		{
			echo 'Group already exists';
		}

		try
		{
			// Create the user
			$user = Sentry::createUser(array(
				'name'     => 'Administrator',
				'email'     => 'admin@custommate.com',
				'password'  => 'adidev',
				'activated' => true,
			));

			// Find the group using the group id
			$adminGroup = Sentry::findGroupById(1);

			// Assign the group to the user
			$user->addGroup($adminGroup);
		}
		catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
		{
			echo 'Login field is required.';
		}
		catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
		{
			echo 'Password field is required.';
		}
		catch (Cartalyst\Sentry\Users\UserExistsException $e)
		{
			echo 'User with this login already exists.';
		}
		catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
		{
			echo 'Group was not found.';
		}
	}

}
