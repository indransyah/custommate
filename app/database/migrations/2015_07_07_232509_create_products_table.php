<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('sku', 5)->unique();
			$table->string('name')->unique();
			$table->string('slug')->unique();
			$table->string('front_photo');
			$table->string('back_photo');
			$table->integer('price');
			$table->integer('weight');
			$table->text('description');
			$table->text('color');
			$table->string('status');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}
