<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddFrontFieldsToProductsTable extends Migration {

    /**
     * Make changes to the table.
     *
     * @return void
     */
    public function up()
    {   
        Schema::table('products', function(Blueprint $table) {     
            
            $table->string('front_file_name')->nullable();
            $table->integer('front_file_size')->nullable();
            $table->string('front_content_type')->nullable();
            $table->timestamp('front_updated_at')->nullable();

        });

    }

    /**
     * Revert the changes to the table.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function(Blueprint $table) {

            $table->dropColumn('front_file_name');
            $table->dropColumn('front_file_size');
            $table->dropColumn('front_content_type');
            $table->dropColumn('front_updated_at');

        });
    }

}
