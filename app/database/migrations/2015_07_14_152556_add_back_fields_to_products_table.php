<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddBackFieldsToProductsTable extends Migration {

    /**
     * Make changes to the table.
     *
     * @return void
     */
    public function up()
    {   
        Schema::table('products', function(Blueprint $table) {     
            
            $table->string('back_file_name')->nullable();
            $table->integer('back_file_size')->nullable();
            $table->string('back_content_type')->nullable();
            $table->timestamp('back_updated_at')->nullable();

        });

    }

    /**
     * Revert the changes to the table.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function(Blueprint $table) {

            $table->dropColumn('back_file_name');
            $table->dropColumn('back_file_size');
            $table->dropColumn('back_content_type');
            $table->dropColumn('back_updated_at');

        });
    }

}
