<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('destination');
			$table->string('origin');
			$table->string('name');
			$table->integer('total');
			$table->text('message');
			$table->dateTime('date');
			$table->integer('invoice_id')->unsigned();
			$table->timestamps();
			$table->foreign('invoice_id')->references('id')->on('invoices');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payments');
	}

}
