<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('shipments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('origin');
			$table->string('destination');
			$table->string('courier');
			$table->string('service');
			$table->integer('cost');
			$table->integer('weight');
			$table->string('tracking_number');
			$table->text('message');
			$table->string('status');
			$table->integer('order_id')->unsigned();
			$table->integer('recipient_id')->unsigned();
			$table->timestamps();
			$table->foreign('order_id')->references('id')->on('orders');
			$table->foreign('recipient_id')->references('id')->on('recipients');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('shipments');
	}

}
