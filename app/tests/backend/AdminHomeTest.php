<?php

class AdminHomeTest extends TestCase {

    public function testIndexIfNoSession()
    {
        /*
         * Tes halaman admin jika belum login
         * */

        Route::enableFilters();
        $this->call('GET', 'cm-admin');
        $this->assertRedirectedToAction('AdminAuthController@getLogin');
    }

    public function testIndexIfHasSession()
    {
        /*
         * Tes halaman admin jika sudah login
         * */
        // $user = new User(array('name' => 'Administrator'));
        // $this->be($user);
        // $this->call('GET', 'cm-admin');
        // $this->assertResponseOk();
    }

}
