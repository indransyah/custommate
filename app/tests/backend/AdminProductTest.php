<?php

class AdminProductTest extends TestCase {

    public function setUp()
    {
        parent::setUp();

        $this->mock = $this->mock('ProductRepository');
        // Set User for view composers
        $user = Sentry::findUserByID(1);
        Sentry::setUser($user);
    }

    public function mock($class)
    {
        $mock = Mockery::mock($class);

        $this->app->instance($class, $mock);

        return $mock;
    }

    public function testGetIndex()
    {
        $this->call('GET', 'cm-admin/product/index');
        $this->assertResponseOk();
    }

    public function testGetAdd()
    {
        $this->call('GET', 'cm-admin/product/add');
        $this->assertResponseOk();
    }

    public function testGetEditWithOutId()
    {
        $this->call('GET', 'cm-admin/product/edit/');
        $this->assertRedirectedToAction('AdminProductController@getIndex');
        $this->assertSessionHas('danger');
    }

    public function testGetEditWithWrongId()
    {
        $this->mock->shouldReceive('find')->once();
        $this->call('GET', 'cm-admin/product/edit/1000');
        $this->assertRedirectedToAction('AdminProductController@getIndex');
        $this->assertSessionHas('danger');
    }

    public function testPostAddValidation()
    {
        $uploadedFile = new Symfony\Component\HttpFoundation\File\UploadedFile('/home/vagrant/Code/Instant Kali Linux.PNG', 'Instant Kali Linux.PNG');
        Input::replace($input = [
                'name' => 'bla bla bla',
                'price' => 100000,
                'front_photo' => $uploadedFile,
                'back_photo' => $uploadedFile,
            ]);
        $this->mock->shouldReceive('store')->once();
        // $this->call('POST', 'cm-admin/product/add/', [], [$input]);
        $this->call('POST', 'cm-admin/product/add/');
        $this->assertRedirectedToAction('AdminProductController@getAdd');
        $this->assertSessionHasErrors();
        // var_dump($uploadedFile);
        // var_dump(ini_get('xdebug.profiler_enable'));
        // $this->assertSessionHasErrors('name');
        // $this->assertSessionHasErrors('price');
        // $this->assertSessionHasErrors('front_photo');
        // $this->assertSessionHasErrors('back_photo');
    }


    // API
    public function testGetProducts()
    {
        $this->mock->shouldReceive('all')->once();

        $this->call('GET', 'cm-admin/product/products');
        $this->assertResponseOk();
    }

}
