<?php

App::bind('ProductInterface', 'ProductRepository');
App::bind('OrderInterface', 'OrderRepository');
App::bind('VectorInterface', 'VectorRepository');
App::bind('AuthInterface', 'AuthRepository');
App::bind('FileInterface', 'FileRepository');
App::bind('RajaOngkirInterface', 'RajaOngkirRepository');
App::bind('UserInterface', 'UserRepository');
App::bind('InvoiceInterface', 'InvoiceRepository');
App::bind('PaymentInterface', 'PaymentRepository');
