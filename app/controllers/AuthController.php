<?php

class AuthController extends BaseController {

  protected $auth;

  public function __construct(AuthInterface $auth)
  {
      $this->beforeFilter('csrf', array('on' => 'post'));
      $this->auth = $auth;
  }

  public function postFacebook()
  {
    $input = Input::all();
    try
    {
      // Cek apakah sudah daftar atau belum jika sudah maka login
      $user = Sentry::findUserByLogin($input['email']);
      Sentry::login($user, true);
      return Response::json(array('message' => 'Login berhasil, selamat datang di Custommate!'), 200);
    }
    catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
    {
      $store = $this->auth->addUser($input);
      if (!$store) {
        return Response::json(array('message' => 'Login gagal!'), 400);
      }
      return Response::json(array('message' => 'Login berhasil, selamat datang di Custommate!'), 200);
    }
  }

  public function getFacebook()
  {
    // get data from input
    $code = Input::get( 'code' );

    // get fb service
    $fb = OAuth::consumer( 'Facebook' );

    // check if code is valid

    // if code is provided get user data and sign in
    if ( !empty( $code ) ) {

      // This was a callback request from facebook, get the token
      $token = $fb->requestAccessToken( $code );

      // Send a request with it
      $result = json_decode( $fb->request( '/me?fields=id,name,email,picture' ), true );

      // $message = 'Your unique facebook user id is: ' . $result['id'] . ' and your name is ' . $result['name'];
      //  echo $message. "<br/>";

      //Var_dump
      //display whole array().
      // dd($result);

      try
      {
        $user = Sentry::findUserByLogin($result['email']);
        Sentry::login($user, true);
        return Redirect::intended('/')->withSuccess('Welcome to Custommate');
      }
      catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
      {
        $result = $this->auth->facebook($result);
        if ($result['status']) {
          return Redirect::intended('/')->withSuccess('Welcome to Custommate');
        }
        return Redirect::action('AuthController@getLogin')
    			->withError($result['message']);
      }

      // $customer = Sentry::findUserByLogin($result['email']);
      // $customer = User::where('facebook_id', $result['id'])->first();

      // if ($customer) {
      //   Sentry::login($customer, true);
      //   return Redirect::intended('/')->withSuccess('Welcome to Custommate');
      // } else {
      //   $result = $this->auth->facebook($result);
      //   if ($result['status']) {
      //     return Redirect::intended('/')->withSuccess('Welcome to Custommate');
      //   }
      //   return Redirect::action('AuthController@getLogin')
    	// 		->withError($result['message']);
      // }

    }
    // if not ask for permission first
    else {
      // get fb authorization
      $url = $fb->getAuthorizationUri();

      // return to facebook login url
      return Redirect::to( (string)$url );
    }
  }

  public function getLogin()
  {
    $title = 'Custommate | Login';
    return View::make('frontend.login', compact('title'));
  }

  public function getLogout()
  {
    Sentry::logout();
    return Response::json(array('message' => 'Anda telah berhasil logout!'), 200);
    // return Redirect::action('AuthController@getLogin')
		// 	->withSuccess('Anda berhasil logout');
  }

}
