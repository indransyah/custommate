<?php

class FileController extends BaseController {

  protected $file;
  protected $product;

  public function __construct(FileInterface $file, ProductInterface $product)
  {
    $this->beforeFilter('customer.api', array('only' => array('postDesign', 'postImage')));
    $this->file = $file;
    $this->product = $product;
  }

  public function postDesign($slug = null)
  {
    if (is_null($slug)) {
      return Response::json(array('message' => 'Cannot upload your design.'), 400);
    }
    $product = $this->product->get($slug);
    if (!$product) {
      return Response::json(array('message' => 'Cannot upload your design.'), 400);
    }
    $input = Input::all();
    $status = $this->file->uploadDesign($input, $product->slug);
    if ($status) {
      Cart::destroy();
      return Response::json(array('message' => 'Design was successfully uploaded.'), 200);
    }
    return Response::json(array('message' => 'Cannot upload your design.'), 400);
  }

  public function postImage()
  {
    $input = Input::all();
    $invalidImage = $this->file->invalidImage($input);
    if (!$invalidImage) {
      $image = Input::file('image');
      $imageName = uniqid().'.'.$image->getClientOriginalExtension();
      $upload = $this->file->uploadImage($image, $imageName);
      if ($upload) {
        $files = array(
          'name' => $imageName,
          'size' => $image->getSize(),
          'url' => URL::to('images/'.$imageName)
        );
        if (Session::has('images')) {
          $images = Session::get('images');
        } else {
          $images = array();
        }
        $images[] = $files;
        Session::put('images', $images);
        return Response::json([ 'files' => array($files)], 200);
      }
       return Response::json([ 'message' => 'Cannot upload your image.'], 400);
    }
    return Response::json([ 'message' => $invalidImage], 400);
  }




  // OLD

  public function getUpload()
  {
    $file = array(['name' => '1'], ['name' => '2']);
    return Response::json([
      'files' => $file,
      'id' => '2']
    );
  }

  public function postCoba() {
    $image = new Gambar;
    $image->image = Input::file('image');
    if ($image->validate()) {
      return 'ok';
    } else {
      return $image->errors();
    }

    // $image->image = Input::file('image');
    // $status = $image->save();
    // if ($status) {
    //   return 'ok';
    // } else {
    //   // dd($image->image);
    //   return $image->errors();
    // }
  }

  public function postNewUpload() {
    $input = Input::all();
    return $input['image'];
    // Build the input for our validation
    $input = array('image' => Input::file('image'));

    // Within the ruleset, make sure we let the validator know that this
    // file should be an image
    $rules = array(
        'image' => 'image'
    );

    // Now pass the input and rules into the validator
    $validator = Validator::make($input, $rules);

    // Check to see if validation fails or passes
    if ($validator->fails())
    {
        // Redirect with a helpful message to inform the user that
        // the provided file was not an adequate type
        return 'Error: The provided file was not an image';
    } else
    {
        // Actually go and store the file now, then inform
        // the user we successfully uploaded the file they chose
        return 'Success: File upload was successful';
    }
  }

  public function postUploadBisa() {
    $uploadedImage = Input::file('image');
    return $uploadedImage;
    // if ($uploadedImage->isValid()) {
    //   return 'valid';
    // }
    // return 'not valid';
    // $results = array();
    $image = new Image;
    $image->file = $uploadedImage;
    if ($image->validate()) {
      $name = $uploadedImage->getClientOriginalName();
      $size = $uploadedImage->getSize();
      $moveStatus = $uploadedImage->move('img/uploads/temp', $name);
      if($moveStatus == false) {
        return Response::json([ 'message' => 'Ups something wrong, please contact administrator!'], 200);
      }
      $files = array(
        'name' => $name,
        'size' => $size,
        'url' => URL::to('img/uploads/temp/'.$name)
      );
      return Response::json([ 'files' => array($files)], 200);
    } else {
        return Response::json([ 'message' => $image->errors()], 400);
      }
		// foreach ($files as $file) {
    //   $image = new image;
    //   $image->files = $file;
    //   if ($image->validate()) {
    //     return Response::json([ 'message' => 'ok'], 200);
    //   } else {
    //     return Response::json([ 'message' => 'nope'], 400);
    //
    //   }
		// 	$name = $file->getClientOriginalName();
    //   $file->move('img/uploads/temp', $name);
    //   $url = 'img/uploads/temp/'.$name;
		// 	$results[] = array(
    //     'name' => $name,
    //     'url' => $url
    //   );
		// }
		// return array(
			// 'files' => $results
		// );

    // $file = Input::file('file');
    // $name = $file->getClientOriginalName();
    // $size = Input::file('file')->getSize();
    // $url = $file->move('img/uploads/temp', $name);
    // return array(
    //   'files' => array(
    //     'name' => $name,
    //     'size' => $size,
    //     'url' => URL::to('img/uploads/temp'.$name),
    //     'thumbnailUrl' => URL::to('img/uploads/temp'.$name),
    //     'deleteUrl' => URL::to('file/delete'),
    //     'deleteType' => 'DELETE'
    //   )
    // );

  //   $surpass = Surpass::path($this->_surpass_test_path);
   //
  //   if($surpass->save($this->attributes)) {
   //
  //     $load_item = $surpass->loadSaved();
  //  $id = $load_item->id;
  //  $dir = $load_item->dir;
  //  $filename = $load_item->filename;
  //  $path = $load_item->path;
  //  $url = $load_item->url;
  //  $attributes = $load_item->attributes;
  //  $tag = $load_item->tag;
   //
  //  $id = $load_item->id;
  //   $surpass->saveAttributes($id, array(
  //       'key_1' => 'value_1',
  //       'key_2' => 'value_2',
  //       'key_3' => 'value_3'
  //   ));
   //
  //   }
   //
  //   return $surpass->result();

  }

}
