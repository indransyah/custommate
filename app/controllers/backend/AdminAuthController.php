<?php

class AdminAuthController extends BaseController {

	public function getIndex()
	{
		return Redirect::action('AdminAuthController@getLogin');
	}

	public function getLogin()
	{
		return View::make('backend.login');
	}

	public function postLogin()
	{
		try
		{
			// Login credentials
			$credentials = array(
				'email'    =>  Input::get('email'),
				'password' =>  Input::get('password'),
			);

			// Authenticate the user
			$user = Sentry::authenticate($credentials, true);
			return Redirect::intended(URL::action('AdminHomeController@getIndex'))
                ->withSuccess('You are now logged in!');
		}
		catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
		{
			return Redirect::action('AdminAuthController@getLogin')
                ->withError('Login field is required.')->withInput();
		}
		catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
		{
			return Redirect::action('AdminAuthController@getLogin')
                ->withError('Password field is required.')->withInput();
		}
		catch (Cartalyst\Sentry\Users\WrongPasswordException $e)
		{
			return Redirect::action('AdminAuthController@getLogin')
                ->withError('Wrong password, try again.')->withInput();
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			return Redirect::action('AdminAuthController@getLogin')
                ->withError('User was not found.')->withInput();
		}
		catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
		{
			return Redirect::action('AdminAuthController@getLogin')
                ->withError('User is not activated.')->withInput();
		}
	}

	public function getLogout()
	{
		Sentry::logout();
    return Redirect::action('AdminAuthController@getLogin')
			->withSuccess('Your are now logged out!');
	}

}
