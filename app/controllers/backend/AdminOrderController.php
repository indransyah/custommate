<?php

class AdminOrderController extends BaseController {

  protected $order;

  public function __construct(OrderInterface $order)
  {
    $this->order = $order;
  }

	public function getIndex()
	{
		$title = 'Orders';
		$desc = 'View Orders';
		return View::make('backend.orders.index', compact('title', 'desc'));
	}

	public function getShow($id)
	{
		$title = 'Order';
		$desc = 'Detail Order';
    $order = $this->order->get($id);
		return View::make('backend.orders.show', compact('title', 'desc', 'order'));
	}

	// API

	public function getAll()
	{
		$data['data'] = $this->order->all();
		return $data;

	}

  public function postValidation()
  {
    if (Input::get('status') === 'valid') {
      return Response::json(array('message' => 'Design status was successfully updated to VALID.'), 200);
    } elseif (Input::get('status') === 'invalid') {
      return Response::json(array('message' => 'Design status was successfully updated to INVALID.'), 200);
    } else {
      return Response::json(array('message' => 'Ups, there is some error. Please try again!'), 400);
    }
  }

  public function postOrder()
  {
    $input = Input::all();
    $status = $this->order->status($input['id'], $input['status']);
    if ($status === false) {
      return Response::json(array('message' => 'Ups, there is some error. Please try again!'), 400);
    }
    return Response::json(array('message' => 'Order status was successfully updated to '.strtoupper($status).'.'), 200);
  }

}
