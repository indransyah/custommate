<?php

class AdminProductController extends BaseController
{

    protected $product;

    public function __construct(ProductInterface $product)
    {
        $this->beforeFilter('csrf', array('on' => 'post'));
        $this->product = $product;
    }

    /**
     * Menampilkan produk-produk
     */
    public function getIndex()
    {
        $title = 'View Products';
        $desc = 'View all products';
        return View::make('backend.products.view', compact('title', 'desc'));
    }

    /**
     * Menampilkan halaman tambah produk
     */
    public function getAdd()
    {
        $title = 'Add Product';
        $desc = 'Add new product';
        return View::make('backend.products.add', compact('title', 'desc'));
    }

    /**
     * Menampilkan halaman edit produk
     */
    public function getEdit($id = NULL)
    {
        if (is_null($id)) {
            return Redirect::action('AdminProductController@getIndex')->withDanger('Ups, cannot edit product without it\'s id!' );
        }
        $product = $this->product->find($id);
        if (!$product) {
            return Redirect::action('AdminProductController@getIndex')->withDanger('Ups, there is no product with that id!' );
        }
        $title = 'Edit Product';
        $desc = 'Edit existing product';
        return View::make('backend.products.edit', compact('title', 'desc', 'product', 'id'));
    }

    /**
     * API
     */

    /**
     * Mengambil data produk untuk ditampilkan pada halaman index produk
     */
    public function getProducts()
    {
        $data['data'] = $this->product->all();
        return $data;
    }

    /**
     * Menghandle tombol publish
     */
    public function postPublish()
    {
        $tableData = Input::get('tableData');
        $response = $this->product->bulkAction($tableData, $status = 'published');
        if (!$response) {
            return Response::json(array('message' => 'Cannot update the product(s)!'), 400);
        }
        return Response::json(array('message' => 'Product(s) was successfully updated'), 200);
    }

    /**
     * Menghandle tombol draft
     */
    public function postDraft()
    {
        $tableData = Input::get('tableData');
        $response = $this->product->bulkAction($tableData, $status = 'draft');
        if (!$response) {
            return Response::json(array('message' => 'Cannot update the product(s)!'), 400);
        }
        return Response::json(array('message' => 'Product(s) was successfully updated'), 200);

    }

    /**
     * Menghandle tombol delete
     */
    public function postDelete()
    {
        $tableData = Input::get('tableData');
        $response = $this->product->bulkDelete($tableData);
        if (!$response) {
            return Response::json(array('message' => 'Cannot delete the product(s)!'), 400);
        }
        return Response::json(array('message' => 'Product(s) was successfully deleted'), 200);
    }

    /**
     * Menghandle form tambah produk
     */
    public function postAdd()
    {
        $input = Input::all();
        $response = $this->product->store($input);
        if (!$response) {
            return Redirect::action('AdminProductController@getAdd')
                ->withError('The following errors occurred')
                ->withErrors($this->product->errors)
                ->withInput();
        }
        return Redirect::action('AdminProductController@getEdit', $response['id'])
            ->withSuccess('A new product was successfully added');
    }

    /**
     * Menghandle form edit produk
     */
    public function postEdit($id)
    {
        $input = Input::all();
        $response = $this->product->update($id, $input);

        if(!$response) {
            return Redirect::action('AdminProductController@getEdit', $id)
                ->withError('The following errors occurred')
                ->withErrors($this->product->errors);
        }
        return Redirect::action('AdminProductController@getEdit', $id)
                ->withSuccess('Product was successfully updated');
    }

    /**
     * Menghandle form tambah warna produk
     */
    public function postColor($id)
    {
        $response = $this->product->addColor($id);
        if (!$response['status']) {
            if ($response['message']=='exist') {
                return Response::json(array('message' => 'Color already exist!'), 400);
            }
            return Response::json(array('message' => 'Invalid color!'), 400);
        }
        return Response::json(array('message' => 'Color successfully added!'), 200);
    }

    /**
     * Menghandle penghapusan warna produk
     */
    public function deleteColor($id)
    {
        $response = $this->product->deleteColor($id);
        if (!$response) {
            return Response::json(array('message' => 'Cannot delete the color!'), 400);
        }
        return Response::json(array('message' => 'Color successfully deleted!'), 200);
    }

    
    // OLD

    // public function postColorOld($id)
    // {
    //     $color = ltrim(Input::get('color'), '#');
    //     if (ctype_xdigit($color) == false && (strlen($color) != 6 || strlen($color) != 3))
    //         return Response::json(array('message' => 'Invalid color!'), 400);

    //     $product = Product::find($id);
    //     $rules = $product::$rules;
    //     unset($rules['front_photo']);
    //     unset($rules['back_photo']);
    //     $temp = array();
    //     if($product->color != null) {
    //         $temp = json_decode($product->color);
    //     }
    //     foreach($temp as $value) {
    //         if($value->color == Input::get('color'))
    //             return Response::json(array('message' => 'Color already exist!'), 400);
    //     }
    //     $temp[] = array('color' => Input::get('color'));
    //     $product->color = json_encode($temp);
    //     $product->updateUniques($rules);
    //     return Response::json(array('message' => 'Color successfully added!'), 200);
    // }

    // public function getColor()
    // {
    //     $product = Product::find(1);
    //     $color = json_decode($product->color);
    //     return $color[0];
    // }


    // public function deleteColorOld($id)
    // {
    //     $product = Product::find($id);
    //     $rules = $product::$rules;
    //     unset($rules['front_photo']);
    //     unset($rules['back_photo']);
    //     $color = Input::get('color');
    //     $colors = json_decode($product->color);
    //     foreach($colors as $key => $value) {
    //         if($color == $value->color)
    //         unset($colors[$key]);
    //     }
    //     $product->color = json_encode(array_values($colors));
    //     $status = $product->updateUniques($rules);
    //     if($status)
    //         return Response::json(array('message' => 'Color successfully deleted!'), 200);
    //     return Response::json(array('message' => 'Cannot delete the color!'), 400);
    // }

    // public function postAction()
    // {
    //     return $input = Input::get('ids');
    //     foreach($input as $id) {
    //         $ok = $id->id;
    //     }
    //     return Response::json(array('message' => $ok) ,200);

    // }

}
