<?php

class AdminHomeController extends BaseController {

	public function getIndex()
	{
		$title = 'Dashboard';
		$desc = 'Custommate\'s dashboard';
		return View::make('backend.home', compact('title', 'desc'));
	}

}
