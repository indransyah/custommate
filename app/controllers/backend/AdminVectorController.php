<?php

class AdminVectorController extends BaseController {

  protected $vector;

  public function __construct(VectorInterface $vector) {
    $this->vector = $vector;
  }

  // API

  public function postValidation() {
    $input = Input::all();
    if ($input['status'] == 'valid' || $input['status'] == 'invalid') {
      $validation = $this->vector->validation($input);
      if ($validation === FALSE) {
        return Response::json(array('message' => 'Ups, there is some error. Please try again!'), 400);
      }
      return Response::json(array('message' => 'Design status was successfully updated.'), 200);
    }
    return Response::json(array('message' => 'Ups, status not alowed!'), 400);
  }

  public function deleteDelete() {
    $input = Input::all();
    $delete = $this->vector->delete($input['id']);
    if ($delete === false)
    {
      return Response::json(array('message' => 'Ups, cannot delete the vector file!'), 400);
    }
    return Response::json(array('message' => 'Vector was successfully deleted.'), 200);
  }

}
