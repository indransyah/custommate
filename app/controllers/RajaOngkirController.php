<?php

class RajaOngkirController extends BaseController {

  protected $rajaongkir;

  public function __construct(RajaOngkirInterface $rajaongkir)
  {
    $this->rajaongkir = $rajaongkir;
  }

  public function getProvince()
  {
    return $this->rajaongkir->province();
  }

  public function getCity($province_id)
  {
    return $this->rajaongkir->city($province_id);
  }

  public function postCost()
  {
    $input = Input::all();
    return $this->rajaongkir->cost($input);
  }

}
