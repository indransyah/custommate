<?php

class ProductController extends BaseController {

	protected $product;

	public function __construct(ProductInterface $product)
	{
		// $this->beforeFilter('customer', array('only' => 'getShow'));
		$this->product = $product;
	}

	public function getIndex() {
		$title = 'Choose your apparel';
		$products = $this->product->allFields();
		return View::make('frontend.products.index', compact('products', 'title'));
	}

	public function getShow($slug = null) {
		$title = 'Customize your apparel';
		if (is_null($slug)) {
			return Redirect::action('ProductController@getIndex');
		}
		$product = $this->product->get($slug);
		if (!$product) {
			return Redirect::action('ProductController@getIndex')->withError('Tidak ada produk yang ditemukan.');
		}
		return View::make('frontend.products.show', compact('product', 'title'));
	}


	// OLD

	public function postDesignOld()
	{
		$design = Input::get('design');
		$design = str_replace('data:image/png;base64,', '', $design);
		$design = base64_decode($design);
		$name = uniqid();
		$file = 'design/'. $name . '.png';
		$status = File::put($file, $design);
		if (!$status) {
            return Response::json(array('message' => 'Cannot upload your design!'), 400);
		}
		$design = array(
				'front' => $name.'.png',
				'back' => $name.'.png'
			);
		Session::put('design', $design);
        return Response::json(array('message' => 'Design was successfully uploaded'), 200);
	}

}
