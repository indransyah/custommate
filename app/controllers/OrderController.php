<?php

class OrderController extends BaseController {

  protected $product;
  protected $order;
  protected $rajaongkir;
  protected $file;

  public function __construct(OrderInterface $order, ProductInterface $product, RajaOngkirInterface $rajaongkir, FileInterface $file)
  {
    // $this->beforeFilter('customer', array('only' => 'getShow'));
    $this->order = $order;
    $this->product = $product;
    $this->rajaongkir = $rajaongkir;
    $this->file = $file;
  }

  public function getCart()
  {
    if (!Session::has('design')) {
      Cart::destroy();
      return Redirect::action('ProductController@getIndex')->withError('Pesanan Anda kosong, silakan pesan produk terlebih dahulu.');
    }
    $title = 'Cart';
    $slug = Session::get('design.product');
    $product = $this->product->get($slug);
    $cart = $this->order->allFields();
    return View::make('frontend.order.cart', compact('product', 'title', 'cart'));
  }

  public function getCartContent()
  {
    $cart = $this->order->allFields();
    return $cart;
  }

  public function getCheckout()
  {
    if (Cart::count() == 0) {
      return Redirect::action('ProductController@getIndex')->withError('Pesanan Anda kosong, silakan pesan produk terlebih dahulu.');
    }
    $title = 'Checkout';
    $slug = Session::get('design.product');
    $product = $this->product->get($slug);
    $cart = $this->order->allFields();
    $province = $this->rajaongkir->province();
    return View::make('frontend.order.checkout', compact('product', 'title', 'cart', 'province'));
  }

  public function getCourier()
  {
    $this->checkCart();
    $title = 'Courier';
    $slug = Session::get('design.product');
    $product = $this->product->get($slug);
    $cart = $this->order->allFields();
    return View::make('frontend.order.courier', compact('product', 'title', 'cart'));
  }

  public function getAdd($slug = null)
  {
    if (is_null($slug)) {
      return Redirect::action('ProductController@getIndex');
    }
    $product = $this->product->get($slug);
    if (!$product) {
      return Redirect::action('ProductController@getIndex')->withError('Tidak ada produk yang ditemukan.');
    }

    // Tidak ada respon dari Cart::add()
    $this->order->add($input, $product);
    return Redirect::action('OrderController@getCart')->withSuccess('Pesanan Anda berhasil ditambahkan ke dalam cart.');

  }

  public function postAdd($slug = null)
  {
    if (is_null($slug)) {
      // return Redirect::action('ProductController@getIndex');
      return Response::json(array('message' => 'Terjadi kesalahan, silakan coba lagi!'), 400);
    }
    $product = $this->product->get($slug);
    if (!$product) {
      // return Redirect::action('ProductController@getIndex')->withError('Tidak ada produk yang ditemukan.');
      return Response::json(array('message' => 'Tidak ada produk yang ditemukan.'), 400);
    }
    $input = Input::all();

    // Tidak ada respon dari Cart::add()
    $this->order->add($input, $product);
    // return Redirect::action('OrderController@getCart')->withSuccess('Pesanan Anda berhasil ditambahkan ke dalam cart.');
    return Response::json(array('message' => 'Pesanan Anda berhasil ditambahkan ke dalam cart.'), 200);
  }

  public function deleteDelete($rowid = null)
  {
    if (is_null($rowid)) {
      return Redirect::action('OrderController@getCart');
    }
    Cart::remove($rowid);
    return Redirect::action('OrderController@getCart')->withSuccess('Pesanan Anda berhasil dihapus.');
  }

  public function deleteClear()
  {
    Cart::destroy();
    return Response::json(array('message' => 'Semua pesanan Anda berhasil dihapus.'), 200);
  }

  public function postCheckout()
  {
    $input = Input::all();
    $validator = $this->order->validator($input);
    if (!$validator) {
      return Redirect::action('OrderController@postCheckout')
          ->withErrors($this->order->errors)
          ->withInput();
    }

    // Get user data
    $customer = Sentry::getUser();

    // Get cart content
    $cart = $this->order->allFields();

    // Get cart total
    $total = Cart::total();

    // Get design file
    $file = Session::get('design');

    // Get product from slug
    $product = $this->product->get($file['product']);

    // Get weight from product * total items in the cart
    $weight = $this->order->weight($product);

    // Get ongkir API (response as JSON)
    $rajaongkir = $this->rajaongkir->detail($input, $weight);

    // Get data from API
    $courier = $rajaongkir->results[0]->code;
    $originCity = $rajaongkir->origin_details->city_name;
    $destinationProvince = $rajaongkir->destination_details->province;
    $destinationCity = $rajaongkir->destination_details->city_name;
    $destinationPostalCode = $rajaongkir->destination_details->postal_code;

    // Get selected courier service from user
    $service = $input['servis'];

    // Get ongkir
    $ongkir = 0;
    foreach ($rajaongkir->results[0]->costs as $key => $value) {
      if ($value->service == $service) {
        $ongkir = $value->cost[0]->value;
      }
    }

    // Sum total + ongkir
    $totalPlusOngkir = $total + $ongkir;

    DB::beginTransaction();
    $order = $this->order->order($customer->id, $product->name, $cart, $total);
    if ($order === false) {
      DB::rollback();
      return Redirect::action('OrderController@postCheckout')
          ->withError('Ups, terjadi kesalahan sehingga order tidak dapat dilakukan. Silakan ulangi kembali!')
          ->withInput();
    }
    $recipient = $this->order->recipient($customer->id, $input, $destinationProvince, $destinationCity, $destinationPostalCode);
    if ($recipient === false) {
      DB::rollback();
      return Redirect::action('OrderController@postCheckout')
          ->withError('Ups, terjadi kesalahan pada data penerima sehingga order tidak dapat dilakukan. Silakan ulangi kembali!')
          ->withInput();
    }
    $shipment = $this->order->shipment($service, $order->id, $recipient->id, $courier, $ongkir, $originCity, $destinationCity, $weight);
    if ($shipment === false) {
      DB::rollback();
      return Redirect::action('OrderController@postCheckout')
          ->withError('Ups, terjadi kesalahan pada data pengiriman sehingga order tidak dapat dilakukan. Silakan ulangi kembali!')
          ->withInput();
    }
    $design = $this->order->design($file, $order->id);
    if ($design === false) {
      DB::rollback();
      return Redirect::action('OrderController@postCheckout')
          ->withError('Ups, terjadi kesalahan pada data desain sehingga order tidak dapat dilakukan. Silakan ulangi kembali!')
          ->withInput();
    }
    $vector = $this->order->vector($input, $design->id);
    if ($vector === false) {
      DB::rollback();
      return Redirect::action('OrderController@postCheckout')
          ->withError('Ups, terjadi kesalahan pada file desain Anda sehingga order tidak dapat dilakukan. Silakan ulangi kembali!')
          ->withInput();
    }
    $invoice = $this->order->invoice($order->id, $totalPlusOngkir);
    if ($invoice === false) {
      DB::rollback();
      return Redirect::action('OrderController@postCheckout')
          ->withError('Ups, terjadi kesalahan ketika membuat invoice / tagihan sehingga order tidak dapat dilakukan. Silakan ulangi kembali!')
          ->withInput();
    }
    DB::commit();
    $this->file->moveDesign($file);
    Session::forget('design');
    Cart::destroy();
    return 'Order Berhasil!';
  }

}
