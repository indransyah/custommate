<?php

class UserController extends BaseController {

  protected $user;
  protected $invoice;
  protected $payment;
  protected $vector;

  public function __construct(UserInterface $user, InvoiceInterface $invoice, PaymentInterface $payment, VectorInterface $vector)
  {
    $this->user = $user;
    $this->invoice = $invoice;
    $this->payment = $payment;
    $this->vector = $vector;
  }

  public function getOrder()
  {
    $customer = Sentry::getUser();
    $order = $this->user->order($customer->id);
    $title = 'Pemesanan';
    $desc = 'Riwayat pemesanan';
    return View::make('frontend.user.order', compact('title', 'desc', 'order'));
  }

  public function getOrderDetail($code)
  {
    $invoice = $this->invoice->getByCode($code);
    if (is_null($invoice)) {
      return Redirect::action('UserController@getOrder');
    }
    $title = 'Pemesanan';
    $desc = 'Detail pemesanan';
    return View::make('frontend.user.order-detail', compact('title', 'desc', 'invoice'));
  }

  public function getInvoice()
  {
    $title = 'Tagihan';
    $desc= 'Daftar Tagihan';
    return View::make('frontend.user.invoice', compact('title', 'desc'));
  }

  public function getPayment()
  {
    $title = 'Pembayaran';
    $desc = 'Konfirmasi Pembayaran';
    $invoice = $this->invoice->lists();
    return View::make('frontend.user.payment', compact('title', 'desc', 'invoice'));
  }

  public function postPayment()
  {
    $input = Input::all();
    $this->payment->add($input);
    return Redirect::action('UserController@getOrder')->withSuccess('Terima kasih, silakan tunggu konfirmasi pembayaran dari kami!');
  }

  public function postVector($code)
  {
    $input = Input::all();
    $validator = $this->vector->validator($input);
    if ($validator === false) {
      return Redirect::action('UserController@getOrderDetail', $code)
          ->withErrors($this->vector->errors)
          ->withInput();
    }
    $invoice = $this->invoice->getByCode($code);
    $upload = $this->vector->upload($invoice->order->design->id, $input);
    if ($upload === false) {
      return Redirect::action('UserController@getOrderDetail', $code)
          ->withError('Ups, tidak dapat mengunggah file!')
          ->withInput();
    }
    return Redirect::action('UserController@getOrderDetail', $code)
      ->withSuccess('File Anda berhasil diunggah, silakan menunggu validasi dari admin!');
  }


}
