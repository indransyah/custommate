<?php

interface RajaOngkirInterface {

  public function province($province_id);

  public function city($province_id, $city_id);

  public function cost($input);

  public function detail($input, $weight);

}
