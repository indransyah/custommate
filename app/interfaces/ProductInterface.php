<?php

interface ProductInterface {

  public function all();

  public function find($id);

  public function store($input);

  public function update($id, $input);

  // FRONTEND
  public function allFields();

  public function get($slug);

}
