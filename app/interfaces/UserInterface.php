<?php

interface UserInterface {

  public function order($user_id);

  public function orderDetail($id);

}
