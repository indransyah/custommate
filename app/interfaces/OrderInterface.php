<?php

interface OrderInterface {

  public function all();

  public function get($id);

  public function status($id, $status);

  // FORNTEND
  public function add($input, $product);

  public function allFields();

  public function weight($product);

  public function validator($input);

  public function order($customer_id, $product_name, $cart, $total);

  public function recipient($customer_id, $input, $province, $city, $postal_code);

  public function shipment($service, $order_id, $recipient_id, $courier, $ongkir, $originCity, $originDestination, $weight);

  public function design($file, $order_id);

  public function vector($input, $design_id);

  public function invoice($order_id, $totalPlusOngkir);

  public function lists();

}
