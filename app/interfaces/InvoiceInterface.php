<?php

interface InvoiceInterface {

  public function lists();

  public function getByCode($code);

}
