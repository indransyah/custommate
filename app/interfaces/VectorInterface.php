<?php

interface VectorInterface {

  public function validation($input);

  public function delete($id);

  // FRONTEND
  public function validator($input);

  public function upload($design_id, $input);

}
