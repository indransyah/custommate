<?php

interface FileInterface {

  public function uploadImage($image, $name);

  public function invalidImage($input);

  public function uploadDesign($input, $slug);

  public function moveDesign($file);

}
