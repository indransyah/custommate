<section class="sidebar">
  <!-- Sidebar Menu -->
  <ul class="sidebar-menu">
    <li class="header">MENU</li>
    <!-- Optionally, you can add icons to the links -->
    <li class="{{ Active::action('AdminHomeController@getIndex'); }}"><a href="#"><i class='fa fa-home'></i> <span>Dashboard</span></a></li>
    <li class="treeview {{ Active::action(['AdminProductController@getIndex', 'AdminProductController@getAdd','AdminProductController@getEdit']); }}">
      <a href="#"><i class='fa fa-barcode'></i> <span>Products</span> <i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <li class="{{ Active::action('AdminProductController@getAdd'); }}"><a href="{{ URL::action('AdminProductController@getAdd') }}">Add product</a></li>
        <li class="{{ Active::action('AdminProductController@getIndex'); }}"><a href="{{ URL::action('AdminProductController@getIndex') }}">View Products</a></li>
      </ul>
    </li>
    <li class="treeview {{ Active::action(['AdminOrderController@getIndex']); }}">
      <a href="#"><i class='fa fa-shopping-cart'></i> <span>Orders</span> <i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <li class="{{ Active::action('AdminOrderController@getIndex'); }}"><a href="{{ URL::action('AdminOrderController@getIndex') }}">View Orders</a></li>
      </ul>
    </li>
    <li class="treeview {{ Active::action(['AdminProductController@getIndex', 'AdminProductController@getAdd','AdminProductController@getEdit']); }}">
      <a href="#"><i class='fa fa-dollar'></i> <span>Payments</span> <i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <li class="{{ Active::action('AdminProductController@getIndex'); }}"><a href="{{ URL::action('AdminProductController@getIndex') }}">View Payments</a></li>
      </ul>
    </li>
  </ul><!-- /.sidebar-menu -->
</section>
