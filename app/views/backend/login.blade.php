<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Custommate - Customize your apparel | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
  <!-- Bootstrap 3.3.4 -->
  <link href="{{ asset('bower_components/admin-lte/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <!-- Font Awesome Icons -->
  <link href="{{ asset('bower_components/components-font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
  <!-- Theme style -->
  <link href="{{ asset('bower_components/admin-lte/dist/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" />

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Engagement">
  <style type="text/css">
  .logo{font-family: Engagement;font-size: 50px;}
  </style>
</head>
<body class="login-page">
  <div class="login-box">
    <div class="login-logo">
      <a href="#" class="logo"><b>Custom</b>mate</a>
    </div><!-- /.login-logo -->
    @include('backend.layouts.alert')
    <div class="login-box-body">
      {{ Form::open(array('action' => 'AdminAuthController@postLogin')) }}
        <div class="form-group has-feedback">
          <input name="email" type="email" class="form-control" placeholder="Email"/>
        </div>
        <div class="form-group has-feedback">
          <input name="password" type="password" class="form-control" placeholder="Password"/>
        </div>
        <div class="row">
          <div class="col-xs-4 pull-right">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
          </div><!-- /.col -->
        </div>
      {{ Form::close() }}
      <a href="#">I forgot my password</a><br>
    </div><!-- /.login-box-body -->
  </div><!-- /.login-box -->

  <!-- jQuery 2.1.4 -->
  <script src="{{ asset('bower_components/admin-lte/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
  <!-- Bootstrap 3.3.2 JS -->
  <script src="{{ asset('bower_components/admin-lte/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
</body>
</html>
