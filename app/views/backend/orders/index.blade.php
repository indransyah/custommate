@extends('backend.layouts.master')

@section('css')
<style>
tr td:first-child {
  text-align: center;
}

tr td:first-child:before {
  content: "\f096"; /* fa-square-o */
  font-family: FontAwesome;
}

tr.selected td:first-child:before {
  content: "\f046"; /* fa-check-square-o */
}
</style>
<link href="{{ asset('bower_components/admin-lte/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('bower_components/admin-lte/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('bower_components/admin-lte/plugins/datatables/jquery.dataTables.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('bower_components/admin-lte/plugins/datatables/extensions/TableTools/css/dataTables.tableTools.css') }}" rel="stylesheet" type="text/css" />
<script src="{{ asset('bower_components/sweetalert/dist/sweetalert.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('bower_components/sweetalert/dist/sweetalert.css') }}">
@stop()

@section('js')
<script src="{{ asset('bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('bower_components/admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('bower_components/admin-lte/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('bower_components/admin-lte/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.js') }}" type="text/javascript"></script>
<script src="{{ asset('bower_components/accounting/accounting.js') }}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('#order').DataTable( {
    "ajax": "{{ URL::action('AdminOrderController@getAll') }}",
    "sort": false,
    "columns": [
      { "data": null, defaultContent: '', orderable: false },
      { "data": "invoice.code" },
      { "data": "user.name" },
      { "data": "invoice.total" },
      { "data": "created_at" },
      { "data": "status" }
    ],
    "dom": 'T<"clear">lfrtip',
    "tableTools": {
      sRowSelect:   'os',
      sRowSelector: 'td:first-child',
      aButtons:     [ 'select_all', 'select_none' ]
    },
    "columnDefs": [
      {
        "targets": 3,
        "render": function ( data, type, row) {
          var rupiah = accounting.formatMoney(data, "Rp ", 2, ".", ","); // €4.999,99
          return rupiah;
        }
      },
      {
        "targets": [1, 5],
        "render": function ( data, type, row ) {
          return data.toUpperCase();
        }
      },
      {
        "targets": 4,
        "render": function ( data, type, row ) {
          var t = data.split(/[- :]/);
          // Apply each element to the Date function
          var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
          return d;
        }
      },
      {
          "targets": 6,
          "data": null,
          "searchable": false,
          "orderable": false,
          "render" :  function ( data, type, row ) {
              return '<a href="{{ URL::action('AdminOrderController@getShow') }}/'+row.id+'" class="btn btn-info btn-sm btn-flat">Detail</a>';
          }
      }]
    });
  } );
  </script>
  @stop()

  @section('content')
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Orders</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <table id="order" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th></th>
                <th>ID</th>
                <th>Name</th>
                <th>Total</th>
                <th>Date</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
          </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
  @stop()
