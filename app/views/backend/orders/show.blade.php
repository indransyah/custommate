@extends('backend.layouts.master')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('bower_components/sweetalert/dist/sweetalert.css') }}">
<style>
.push-top{
  margin-top: 0px;
}
.dl-horizontal dd{
  margin-left: 120px;
}
.dl-horizontal dt{
  width: 100px;
}
textarea.form-control{
  width: inherit;
}
</style>
@stop()

@section('js')
<script src="{{ asset('bower_components/sweetalert/dist/sweetalert.min.js') }}"></script>
<script type="text/javascript">
$('#resi').change(function() {
  alert('ok');
});
$('.validation').click(function() {
  id = $(this).data("id");
  status = $(this).data("status");
  swal({
    title: "Any message?",
    text: "Write your message here:",
    type: "input",
    showCancelButton: true,
    closeOnConfirm: false,
    animation: "slide-from-top",
    inputPlaceholder: "Write something",
    showLoaderOnConfirm: true,
  },
  function(inputValue){
    if (inputValue === false) return false;

    if (inputValue === "") {
      swal.showInputError("You need to write something!");
      return false
    }

    $.post("{{URL::action('AdminVectorController@postValidation')}}", { id: id, message: inputValue, status: status })
    .done(function(data) {
      window.location.reload();
      // swal({
      //   title: "Success!",
      //   text: data.message,
      //   type: "success",
      //   allowOutsideClick: true,
      // });
    })
    .fail(function(data) {
      swal({
        title: "Error!",
        text: data.responseJSON.message,
        type: "error",
        allowOutsideClick: true,
      });
    });
  });
});
$('#delete').click(function() {
  id = $(this).data("id");
  swal({
    title: "Are you sure?",
    text: "You will not be able to recover this vector file!",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Delete",
    closeOnConfirm: false
  },
  function(){
    $.post("{{URL::action('AdminVectorController@deleteDelete')}}", { id: id, _method: 'delete' })
    .done(function(data) {
      window.location.reload();
    })
    .fail(function(data) {
      swal({
        title: "Error!",
        text: data.responseJSON.message,
        type: "error",
        allowOutsideClick: true,
      });
    });
  });
});
$('.order').click(function() {
  id = {{ $order->id }};
  status = $(this).data("status");
  $.post("{{URL::action('AdminOrderController@postOrder')}}", { id: id, status: status })
  .done(function(data) {
    window.location.reload();
  })
  .fail(function(data) {
    swal({
      title: "Error!",
      text: data.responseJSON.message,
      type: "error",
      allowOutsideClick: true,
    });
  });
});
</script>
@stop()

@section('content')
<div class="row">
  <div class="col-md-6">

    <div class="box box-solid">
      <div class="box-header with-border">
        <i class="fa fa-shopping-cart"></i>
        <h3 class="box-title">Order</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col-md-6">
            <h3 class="push-top">Order</h3>
            <dl class="dl-horizontal">
              <dt>Name</dt>
              <dd>{{ $order->user->name }}</dd>
              <dt>Email</dt>
              <dd>{{ $order->user->email }}</dd>
              <dt>Date</dt>
              <dd>{{ $order->created_at->toDayDateTimeString() }}</dd>
              <dt>Status</dt>
              <dd>
                <div class="btn-group">
                  <button type="button" class="btn btn-default btn-flat btn-sm">{{ strtoupper($order->status) }}</button>
                  <button type="button" class="btn btn-default btn-flat btn-sm dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a class="order" data-status="upload">Upload</a></li>
                    <li><a class="order" data-status="validasi">Validasi</a></li>
                    <li><a class="order" data-status="pembayaran">Pembayaran</a></li>
                    <li><a class="order" data-status="konfirmasi">Konfirmasi</a></li>
                    <li><a class="order" data-status="produksi">Produksi</a></li>
                    <li><a class="order" data-status="pengiriman">Pengiriman</a></li>
                    <li><a class="order" data-status="selesai">Selesai</a></li>
                  </ul>
                </div>
              </dd>
            </dl>
          </div>

          <div class="col-md-6">
            <h3 class="push-top">Invoice</h3>
            <dl class="dl-horizontal">
              <dt>Code</dt>
              <dd>{{ strtoupper($order->invoice->code) }}</dd>
              <dt>Total</dt>
              <dd>{{ Helpers::rupiah($order->invoice->total) }}</dd>
              <dt>Status</dt>
              <dd>
                <div class="btn-group">
                  <button type="button" class="btn btn-default btn-flat btn-sm">Action</button>
                  <button type="button" class="btn btn-default btn-flat btn-sm dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Lunas</a></li>
                    <li><a href="#">Belum Lunas</a></li>
                  </ul>
                </div>
              </dd>
            </dl>
          </div>

        </div>

        <h3>Product</h3>
        <p><strong>{{ $order->product_name }}</strong></p>
        <table class="table table-bordered">
          <tbody>
            <tr>
              <th>Price</th>
              <th>Size</th>
              <th>Qty</th>
              <th>Subtotal</th>
            </tr>
            @foreach($order->product as $key => $product)
            <tr>
              <td>{{ Helpers::rupiah($product->pivot->price) }}</td>
              <td>{{ $product->pivot->size }}</td>
              <td>{{ $product->pivot->qty }}</td>
              <td>{{ Helpers::rupiah($product->pivot->subtotal) }}</td>
            </tr>
            @endforeach
            <tr>
              <td colspan="3"><strong>Total</strong></td>
              <td>{{ Helpers::rupiah($order->total) }}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>

  <div class="col-md-6">
    <div class="box box-solid">
      <div class="box-header with-border">
        <i class="fa fa-shopping-cart"></i>
        <h3 class="box-title">Shipment</h3>
      </div>
      <div class="box-body">
        <h3>Recipient</h3>
        <dl class="dl-horizontal">
          <dt>Name</dt>
          <dd>{{ $order->shipment->recipient->name }}</dd>
          <dt>Phone</dt>
          <dd>{{ $order->shipment->recipient->phone }}</dd>
          <dt>Address</dt>
          <dd>{{ $order->shipment->recipient->address }} {{ $order->shipment->recipient->postal_code }}</dd>
        </dl>
        <h3 class="push-top">Shipment</h3>
        <dl class="dl-horizontal">
          <dt>Courier</dt>
          <dd>{{ strtoupper($order->shipment->courier) }}</dd>
          <dt>Service</dt>
          <dd>{{ strtoupper($order->shipment->service) }}</dd>
          <dt>Cost</dt>
          <dd>{{ Helpers::rupiah($order->shipment->cost) }}</dd>
          <dt>Weight</dt>
          <dd>{{ $order->shipment->weight }} gram</dd>
          <dt>Origin</dt>
          <dd>{{ $order->shipment->origin }}</dd>
          <dt>Destination</dt>
          <dd>{{ $order->shipment->destination }}</dd>
          <dt>Tracking Num</dt>
          <dd>
            <div class="input-group">
              <input class="form-control" type="text" disabled="disabled">
              <span class="input-group-btn">
                <button class="btn btn-info btn-flat" type="button"><i class="fa fa-pencil"></i></button>
              </span>
            </div>
          </dd>
        </dl>
      </div>
    </div>
  </div>
</div><!-- /.row -->

<div class="row">
  <div class="col-md-12">
    <div class="box box-danger">
      <div class="box-header">
        <i class="fa fa-file"></i>
        <h3 class="box-title">Design</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body table-responsive">
        <div class="row">
          <div class="col-md-6">
            <img class="col-md-6 img-responsive" src="{{ URL::to('designs/'.$order->design->front) }}">
            <img class="col-md-6 img-responsive" src="{{ URL::to('designs/'.$order->design->back) }}">
          </div>
          <div class="col-md-6">
            <table class="table table-bordered">
              <tbody>
                <tr>
                  <th>Date</th>
                  <th>Download</th>
                  <th>Message</th>
                  <th>Status / Action</th>
                </tr>
                @foreach($order->design->vector as $key => $vector)
                <tr>
                  <td>{{ $vector->created_at->toDayDateTimeString() }}</td>
                  <td class="text-center"><a href="{{ URL::to($vector->path.$vector->name) }}"><span class="fa fa-download"></span></a></td>
                  <td>{{ ($vector->message == '') ? '-' : $vector->message }}</td>
                  <td>
                    <div class="btn-group">
                      <button type="button" class="btn {{ ($vector->status == 'valid') ? 'btn-success' : (($vector->status == 'invalid') ? 'btn-danger' : 'btn-default') }} btn-flat">{{ ($vector->status == '') ? 'Action' : strtoupper($vector->status) }}</button>
                      <button type="button" class="btn {{ ($vector->status == 'valid') ? 'btn-success' : (($vector->status == 'invalid') ? 'btn-danger' : 'btn-default') }} btn-flat dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                      </button>
                      <ul class="dropdown-menu" role="menu">
                        <li><a class="validation" data-id="{{ $vector->id }}" data-status="valid">Valid</a></li>
                        <li><a class="validation" data-id="{{ $vector->id }}" data-status="invalid">Invalid</a></li>
                        <li><a id="delete" data-id="{{ $vector->id }}">Delete</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>
                @endforeach
                @if($order->design->vector->count() == 0)
                <tr>
                  <td colspan="4"><p class="text-center">No vector, please change the Order status to UPLOAD!</p></td>
                </tr>
                @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
</div>
@stop()
