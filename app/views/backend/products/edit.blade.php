@extends('backend.layouts.master')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('bower_components/sweetalert/dist/sweetalert.css') }}">
<link href="{{ asset('bower_components/admin-lte/plugins/colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('bower_components/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{ asset('bower_components/jquery-loading/dist/jquery.loading.css') }}" type="text/css">
<style type="text/css">
  .color-preview {
    border: 1px solid #CCC;
    margin: 2px;
    vertical-align: top;
    display: inline-block;
    cursor: pointer;
    overflow: hidden;
    width: 20px;
    height: 20px;
  }
</style>
@stop()

@section('js')
<script src="{{ asset('bower_components/admin-lte/plugins/colorpicker/bootstrap-colorpicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('bower_components/sweetalert/dist/sweetalert.min.js') }}"></script>
<script src="{{ asset('bower_components/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script src="{{ asset('bower_components/jquery-loading/dist/jquery.loading.js') }}"></script>
<script type="text/javascript">
$.ajaxSetup({ headers: { 'X-CSRF-TOKEN' : '{{ csrf_token() }}' } });

$(document).ajaxSend(function(event, request, settings) {
  $('body').loading();
});

$(document).ajaxComplete(function(event, request, settings) {
  $('body').loading('stop');
});

function convertToHex() {
  document.getElementById('chosen-color').value = document.getElementById('color1').value;
}

function toHex(rgbColor) {
  var parts = rgbColor.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
  delete(parts[0]);
  for (var i = 1; i <= 3; ++i) {
    parts[i] = parseInt(parts[i]).toString(16);
    if (parts[i].length == 1) parts[i] = '0' + parts[i];
  }
  return '#' + parts.join('');
}

$(function () {
  $(document).on('dblclick', '.color-preview', function() {
    var color = toHex($(this).css('backgroundColor'));
    var item = $(this);
    swal({
      title: "Are you sure to delete the color?",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: true,
      allowOutsideClick: true,
    },
    function (isConfirm) {
      if (isConfirm) {
        var data = {
          '_token': $('input[name=_token]').val(),
          '_method': 'delete',
          'color': color
        }
        $.ajax({
          method: "POST",
          url: "{{ URL::action('AdminProductController@deleteColor', $id)  }}",
          data: data,
          dataTpe: "json",
          success: function (d) {
            item.remove();
            swal(d.message, null, "success");
          },
          error: function (e) {
            swal(e.responseJSON.message, null, "error");
          }
        });

      }
    });

  });
  $("#addColor").on('click', function () {
    var data = {
      // '_token': $('input[name=_token]').val(),
      'color': $('input[name=color]').val()
    }
    $.ajax({
      method: "POST",
      url: "{{ URL::action('AdminProductController@postColor', $id) }}",
      data: data,
      dataType: "json",
      success: function (d) {
        console.log(d.message);
        $('<li/>').attr('class', 'color-preview pull-left').attr('style', 'background-color:' + data.color).appendTo('#navColor');
        swal({
          title: "Sukses!",
          text: d.message,
          type: "success",
          timer: 2000,
          showConfirmButton: false,
          allowOutsideClick: true,
        });
      },
      error: function (d) {
        console.log(d.responseJSON.message);
        swal({
          title: "Error!",
          text: d.responseJSON.message,
          type: "error",
          timer: 2000,
          showConfirmButton: false,
          allowOutsideClick: true,
        });
      }
    });
  });
  $("textarea").wysihtml5();
});
</script>
@stop()

@section('content')
<div data-alerts="alerts" data-titles="{'warning': '<em>Warning!</em>'}" data-ids="myid" data-fade="3000"></div>
<div class="row">
  <!-- left column -->
  <div class="col-md-6">
    <!-- Horizontal Form -->
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title">Add Product</h3>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
      {{ Form::open(array('action' => array('AdminProductController@postEdit', $product->id), 'files' => true, 'class' => 'form-horizontal')) }}
      <div class="box-body">
        <div class="form-group">
          <label for="name" class="col-sm-2 control-label">Name *</label>

          <div class="col-sm-10">
            {{ Form::text('name', $product->name, ['id' => 'name', 'class' => 'form-control', 'placeholder' => 'Name']) }}
          </div>
        </div>
        <div class="form-group">
          <label for="price" class="col-sm-2 control-label">Price *</label>

          <div class="col-sm-10">
            {{ Form::text('price', $product->price, ['id' => 'price', 'class' => 'form-control', 'placeholder' => 'Price']) }}
          </div>
        </div>
        <div class="form-group">
          <label for="front" class="col-sm-2 control-label">Front Photo *</label>

          <div class="col-sm-10">
            {{ Form::file('front_photo') }}
            <p><small><i>530px x 630px .png image with transparet background</i></small></p>
          </div>
        </div>
        <div class="form-group">
          <label for="back" class="col-sm-2 control-label">Back Photo *</label>

          <div class="col-sm-10">
            {{ Form::file('back_photo') }}
            <p><small><i>530px x 630px .png image with transparet background</i></small></p>
          </div>
        </div>
        <div class="form-group">
          <label for="weight" class="col-sm-2 control-label">Weight *</label>
          <div class="col-sm-10">
            {{ Form::text('weight', $product->weight, ['id' => 'weight', 'class' => 'form-control', 'placeholder' => 'Weight (grams)']) }}
          </div>
        </div>
        <div class="form-group">
          <label for="color" class="col-sm-2 control-label">Color *</label>

          <div class="col-sm-10">
            {{--<div style="margin-top: 10px;">--}}
              <input type="color" name="color" id="color" class="pull-left">
              <a id="addColor" class="btn btn-default btn-flat btn-xs pull-left"
              style="margin-left: 5px;"><i class="fa fa-plus"></i> Add Color</a>
              {{--</div>--}}
              <br>

              <div class="col-md-12 no-padding">
                <ul class="nav" id="navColor">
                  @if($product->color != null)
                  @foreach(json_decode($product->color) as $value)
                  <li class="color-preview pull-left"
                  style="background-color: {{ $value->color }};"></li>
                  @endforeach
                  @endif
                </ul>
              </div>
              <p><small><i>Double click the color to delete</i></small></p>
            </div>
          </div>
          <div class="form-group">
            <label for="description" class="col-sm-2 control-label">Description</label>
            <div class="col-sm-10">
              {{ Form::textarea('description', $product->description, ['id' => 'description', 'class' => 'form-control']) }}
            </div>
          </div>
          <i>* required</i>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <button type="submit" class="btn btn-info pull-right">Save</button>
        </div>
        <!-- /.box-footer -->
        {{ Form::close() }}
      </div>
      <!-- /.box -->
    </div>
    <!--/.col (left) -->
    <!-- right column -->
    <div class="col-md-6">
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Photo</h3>
        </div>
        <!-- /.box-header -->
        <div class="row text-center">
          <div class="col-md-6">
            <img class="img-responsive" src="{{ $product->front->url() }}">
            <strong>Front</strong>
          </div>
          <div class="col-md-6">
            <img class="img-responsive" src="{{ $product->back->url() }}">
            <strong>Back</strong>
          </div>
        </div>
      </div>
      <!-- /.box -->
    </div>
    <!--/.col (right) -->
  </div>   <!-- /.row -->
  @stop()
