@extends('backend.layouts.master')

@section('css')
<style>
tr td:first-child {
    text-align: center;
}

tr td:first-child:before {
    content: "\f096"; /* fa-square-o */
    font-family: FontAwesome;
}

tr.selected td:first-child:before {
    content: "\f046"; /* fa-check-square-o */
}
#product_filter {
    margin-right: 3px;
}
.DTTT_disabled {
    pointer-events: none;
}
</style>
<link href="{{ asset('bower_components/admin-lte/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('bower_components/admin-lte/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('bower_components/admin-lte/plugins/datatables/jquery.dataTables.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('bower_components/admin-lte/plugins/datatables/extensions/TableTools/css/dataTables.tableTools.css') }}" rel="stylesheet" type="text/css" />
<script src="{{ asset('bower_components/sweetalert/dist/sweetalert.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('bower_components/sweetalert/dist/sweetalert.css') }}">
@stop()

@section('js')
<script src="{{ asset('bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('bower_components/admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('bower_components/admin-lte/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('bower_components/admin-lte/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.js') }}" type="text/javascript"></script>
<script src="{{ asset('bower_components/accounting/accounting.js') }}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    $.ajaxSetup({ headers: { 'X-CSRF-TOKEN' : '{{ csrf_token() }}' } });
    var table = $('#product').DataTable({
        dom: 'T<"clear">lfrtip',
        tableTools: {
            "sRowSelect" : 'multi',
            "sRowSelector" : 'td:first-child',
            "aButtons": [{
                "sExtends":    "ajax",
                "sButtonText": "Publish",
                "bSelectedOnly": true,
                "sAjaxUrl": "{{ URL::action('AdminProductController@postPublish')  }}",
                "mColumns": [1],                            
                "bHeader" : false,
                "sNewLine": ",",
                "sButtonClass": "publish DTTT_disabled",
                "fnAjaxComplete": function ( XMLHttpRequest, textStatus ) {
                    table.ajax.reload( null, false );
                    swal({
                        title: XMLHttpRequest.message,
                        type: "success",
                        timer: 2000,
                        showConfirmButton: false,
                        allowOutsideClick: true,
                    });
                },
            },
            {
                "sExtends":    "ajax",
                "sButtonText": "Draft",
                "bSelectedOnly": true,
                "sAjaxUrl": "{{ URL::action('AdminProductController@postDraft')  }}",
                "mColumns": [1],                            
                "bHeader" : false,
                "sNewLine": ",",
                "sButtonClass": "draft DTTT_disabled",
                "fnAjaxComplete": function ( XMLHttpRequest, textStatus ) {
                    table.ajax.reload( null, false );
                    swal({
                        title: XMLHttpRequest.message,
                        type: "success",
                        timer: 2000,
                        showConfirmButton: false,
                        allowOutsideClick: true,
                    });
                }
            },
            {
                "sExtends":    "ajax",
                "sButtonText": "Delete",
                "bSelectedOnly": true,
                "sAjaxUrl": "{{ URL::action('AdminProductController@postDelete')  }}",
                "mColumns": [1],                            
                "bHeader" : false,
                "sNewLine": ",",
                "sButtonClass": "delete DTTT_disabled",
                "fnAjaxComplete": function ( XMLHttpRequest, textStatus ) {
                    table.ajax.reload( null, false );
                    swal({
                        title: XMLHttpRequest.message,
                        type: "success",
                        timer: 2000,
                        showConfirmButton: false,
                        allowOutsideClick: true,
                    });
                },
                "fnClick": function( nButton, oConfig ) {
                    var data = this.fnGetTableData(oConfig); 
                    var url = oConfig.sAjaxUrl;
                    var ajaxComplete = oConfig.fnAjaxComplete;
                    swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this imaginary file!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    }, function(){
                        $.ajax({
                            "url": url,
                            "data": [{
                                "name": "tableData",
                                "value": data
                            }],
                            "success": ajaxComplete,
                            "dataType": "JSON",
                            "type": "POST",
                            "cache": false,
                            "error": function (d) {
                                swal({
                                    title: d.responseJSON.message,
                                    type: "error",
                                    timer: 2000,
                                    showConfirmButton: false,
                                    allowOutsideClick: true,
                                });
                            } 
                        });
                    });
                }
            }]
        },
        "ajax": "{{ URL::action('AdminProductController@getProducts')  }}",
        "columns": [
        { "data": null, defaultContent: '', orderable: false },
        { "data": "id" },
        { "data": "sku" },
        { "data": "name" },
        { "data": "price" },
        { "data": "slug" },
        { "data": "status" }
        ],
        "sort": false,
        "columnDefs": [
        {
            "targets": 6,
            "render": function ( data, type, row ) {
                return data.toUpperCase();
            }
        },
        {
            "targets": 4,
            "render": function ( data, type, row) {
                var rupiah = accounting.formatMoney(data, "Rp ", 2, ".", ","); // €4.999,99
                return rupiah;
            }
        },
        {
            "targets": [1],
            "visible": false,
            "searchable": false
        },
        {
            'targets': 7,
            'data': null,
            'width': '80px',
            'searchable': false,
            'orderable': false,
            'render' :  function ( data, type, row ) {
                return "<a href='{{ URL::action('AdminProductController@getEdit') }}/"+row.id+"' class='btn btn-info btn-sm btn-flat'>Edit</a>";
            }
        }],
    });
    $('#product tbody').on('click', 'tr', function() {
        if ($(this).hasClass('selected')) {
            $('.publish, .draft, .delete').removeClass("DTTT_disabled");
        } else {
            table.$('tr.selected').removeClass('selected');
            $('.publish, .draft, .delete').addClass("DTTT_disabled");
        }
    });
});
</script>
@stop()

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Products</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="product" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th></th>
                            <th>ID</th>
                            <th>SKU</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Slug</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->
@stop()
