@extends('backend.layouts.master')

@section('css')
    <style>
        .toolbar {
            float: left;
        }
        .dataTables_filter {
            float: right;
        }
    </style>
    <link href="{{ asset('bower_components/admin-lte/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('bower_components/admin-lte/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}" rel="stylesheet" type="text/css" />
@stop()

@section('js')
    <script src="{{ asset('bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bower_components/admin-lte/plugins/datatables/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bower_components/admin-lte/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var table = $('#products').DataTable({
                "dom": '<"toolbar">frtip',
                "ajax": "{{ URL::action('AdminProductController@getProducts')  }}",
                "columns": [
                    { "data": "id" },
                    { "data": "sku" },
                    { "data": "name" },
                    { "data": "price" },
                    { "data": "slug" },
                    { "data": "status" }
                ],
                "sort": false,
                'columnDefs': [{
                    'targets': 0,
                    'width': '5px',
                    'searchable':false,
                    'orderable':false,
                    'className': 'dt-body-center',
                    'render': function (data, type, full, meta){
                        return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
                    }
                }, {
                    'targets': 6,
                    'data': null,
                    'width': '80px',
                    'searchable': false,
                    'orderable': false,
                    'render' :  function ( data, type, row ) {
                        return "<a href='{{ URL::action('AdminProductController@getEdit') }}/"+row.id+"' class='btn btn-info btn-sm btn-flat'>Edit</a><a class='btn btn-danger btn-sm btn-flat'>Delete</a>";
                    }
//                    'defaultContent': '<button type="button" class="btn btn-primary btn-details">Edit'+this.row(0).data()+'</button>'
                }],
                "responsive": true
//                "paging": true,
//                "lengthChange": false,
//                "searching": false,
//                "ordering": true,
//                "info": true,
//                "autoWidth": false
            });

//            $("div.toolbar").html('<strong>Action </strong><select><option value="1">Delete</option><option value="2">Publish</option></select>');
            $("div.toolbar")
//                    .append($('<strong/>').text('Action '))
                    .append(
                        $('<select/>').attr('name', 'action').attr('autocomplete', 'off').attr('class', 'form-control input-sm pull-left')
                                .append($('<option/>').val('delete').text('Delete'),$('<option/>').val('publish').text('Publish'),$('<option/>').val('draft').text('Draft')))
                    .append($('<button/>').attr('id', 'action').text('Apply').attr('class', 'btn btn-info btn-flat btn-sm pull-left'));


            // Handle click on "Select all" control
            $('#example-select-all').on('click', function(){
                // Check/uncheck all checkboxes in the table
                var rows = table.rows({ 'search': 'applied' }).nodes();
                $('input[type="checkbox"]', rows).prop('checked', this.checked);
            });

            // Handle click on checkbox to set state of "Select all" control
            $('#products tbody').on('change', 'input[type="checkbox"]', function(){
                // If checkbox is not checked
                if(!this.checked){
                    var el = $('#example-select-all').get(0);
                    // If "Select all" control is checked and has 'indeterminate' property
                    if(el && el.checked && ('indeterminate' in el)){
                        // Set visual state of "Select all" control
                        // as 'indeterminate'
                        el.indeterminate = true;
                    }
                }
            });

            // Handle form submission event
            $('#frm-example').on('submit', function(e){
                var form = this;

                // Iterate over all checkboxes in the table
                table.$('input[type="checkbox"]').each(function(){
                    // If checkbox doesn't exist in DOM
                    if(!$.contains(document, this)){
                        // If checkbox is checked
                        if(this.checked){
                            // Create a hidden element
                            $(form).append(
                                    $('<input>')
                                            .attr('type', 'hidden')
                                            .attr('name', this.name)
                                            .val(this.value)
                            );
                        }
                    }
                });
            });

            $(document).on('click', '#action', function() {
//                var data = table.$('input[type="checkbox"]').serialize();
//                console.log(data);
                var data = {
                    'ids': table.$('input[type="checkbox"]').serializeArray(),
                    '_token': '{{ csrf_token() }}'
                }
                console.log(data);
                $.ajax({
                    method: "POST",
                    url: "{{ URL::action('AdminProductController@postAction') }}",
                    data: data,
                    dataType: "json",
                    success: function (d) {
                        console.log(d.message);
//                        $('<li/>').attr('class', 'color-preview pull-left').attr('style', 'background-color:' + data.color).appendTo('#navColor');
//                        swal({
//                            title: d.message,
//                            type: "success",
//                            timer: 3000,
//                            showConfirmButton: false
//                        });
                    },
                    error: function (d) {
                        console.log(d.responseJSON.message);
//                        swal({
//                            title: d.responseJSON.message,
//                            type: "error",
//                            timer: 5000,
//                            showConfirmButton: false
//                        });
                    }
                });

            });
        });
    </script>
@stop()

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Products</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <table id="products" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th><input autocomplete="off" name="select_all" value="1" id="example-select-all" type="checkbox"></th>
                            <th>SKU</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Slug</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
@stop()
