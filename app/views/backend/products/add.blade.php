@extends('backend.layouts.master')

@section('css')
<link href="{{ asset('bower_components/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" type="text/css" />
@stop()

@section('js')
<script src="{{ asset('bower_components/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script>
  $(function () {
    $("textarea").wysihtml5();
  });
</script>
@stop()

@section('content')
<div class="row">
  <!-- left column -->
  <div class="col-md-6">
    <!-- Horizontal Form -->
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title">Add Product</h3>
      </div><!-- /.box-header -->
      <!-- form start -->
        {{ Form::open(array('action' => 'AdminProductController@postAdd', 'files' => true, 'class' => 'form-horizontal')) }}
        <div class="box-body">
          <div class="form-group">
            <label for="name" class="col-sm-2 control-label">Name *</label>
            <div class="col-sm-10">
              {{ Form::text('name', null, ['id' => 'name', 'class' => 'form-control', 'placeholder' => 'Name']) }}
            </div>
          </div>
          <div class="form-group">
            <label for="price" class="col-sm-2 control-label">Price *</label>
            <div class="col-sm-10">
              {{ Form::text('price', null, ['id' => 'price', 'class' => 'form-control', 'placeholder' => 'Price']) }}
            </div>
          </div>
          <div class="form-group">
            <label for="front" class="col-sm-2 control-label">Front Photo *</label>
            <div class="col-sm-10">
              {{ Form::file('front_photo') }}
              <p><small><i>530px x 630px .png image with transparet background</i></small></p>
            </div>
          </div>
          <div class="form-group">
            <label for="back" class="col-sm-2 control-label">Back Photo *</label>
            <div class="col-sm-10">
              {{ Form::file('back_photo') }}
              <p><small><i>530px x 630px .png image with transparet background</i></small></p>
            </div>
          </div>
          <div class="form-group">
            <label for="weight" class="col-sm-2 control-label">Weight *</label>
            <div class="col-sm-10">
              {{ Form::text('weight', null, ['id' => 'weight', 'class' => 'form-control', 'placeholder' => 'Weight (grams)']) }}
            </div>
          </div>
          <div class="form-group">
            <label for="color" class="col-sm-2 control-label">Color</label>
            <div class="col-sm-10">
              <p class="help-block">You cannot add a color before save the product first!</p>
            </div>
          </div>
          <div class="form-group">
            <label for="description" class="col-sm-2 control-label">Description</label>
            <div class="col-sm-10">
              {{ Form::textarea('description', null, ['id' => 'description', 'class' => 'form-control']) }}
            </div>
          </div>
          <i>* required</i>

        </div><!-- /.box-body -->
        <div class="box-footer">
          <button type="submit" class="btn btn-info pull-right">Save</button>
        </div><!-- /.box-footer -->
      {{ Form::close() }}
    </div><!-- /.box -->
  </div><!--/.col (left) -->
</div>   <!-- /.row -->
@stop()
