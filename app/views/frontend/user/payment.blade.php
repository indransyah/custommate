@extends('frontend.layouts.master')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ URL::to('bower_components/datetimepicker/jquery.datetimepicker.css') }}">
@stop

@section('top-js')

@stop

@section('js')
<script src="{{ URL::to('bower_components/datetimepicker/jquery.datetimepicker.js') }}"></script>
<script>
$('.date').datetimepicker();
</script>
@stop

@section('content')
@include('frontend.user.sidebar')
<div class="col-lg-9">
  <div class="panel panel-default">
    <div class="panel-heading">{{ $desc }}</div>
    <div class="panel-body">
      @if(empty($invoice))
      <p class="text-center">Maaf, Anda tidak memiliki pesanan dalam tahap pembayaran.</p>
      @else
      {{ Form::open(array('action' => 'UserController@postPayment', 'class' => 'form-horizontal')) }}
        <div class="form-group">
          <label for="invoice" class="col-sm-2 control-label">Invoice ID</label>
          <div class="col-sm-10">
            {{ Form::select('invoice', $invoice, null, ['class' => 'form-control', 'required' => 'required']) }}
          </div>
        </div>
        <div class="form-group">
          <label for="destination" class="col-sm-2 control-label">Bank Tujuan</label>
          <div class="col-sm-10">
            {{ Form::select('bank-tujuan', ['mandiri' => 'Mandiri | 137-00-0751607-9 | Anugerah Chandra Utama', 'bca' => 'BCA | 0373323672 | Anugerah Chandra Utama'], null, ['class' => 'form-control', 'required' => 'required']) }}
          </div>
        </div>
        <div class="form-group">
          <label for="origin" class="col-sm-2 control-label">Bank Asal</label>
          <div class="col-sm-10">
            {{ Form::text('bank-asal', null, ['class' => 'form-control', 'placeholder' => 'Bank Asal', 'required' => 'required']) }}
          </div>
        </div>
        <div class="form-group">
          <label for="name" class="col-sm-2 control-label">Atas Nama</label>
          <div class="col-sm-10">
            {{ Form::text('nama', null, ['class' => 'form-control', 'placeholder' => 'Nama Pengirim', 'required' => 'required']) }}
          </div>
        </div>
        <div class="form-group">
          <label for="total" class="col-sm-2 control-label">Jumlah</label>
          <div class="col-sm-10">
            {{ Form::text('jumlah', null, ['class' => 'form-control', 'placeholder' => 'Jumlah', 'required' => 'required']) }}
          </div>
        </div>
        <div class="form-group">
          <label for="date" class="col-sm-2 control-label">Tanggal</label>
          <div class="col-sm-10">
            {{ Form::text('tanggal', null, ['class' => 'date form-control', 'placeholder' => 'Tanggal', 'required' => 'required']) }}
          </div>
        </div>
        <div class="form-group">
          <label for="message" class="col-sm-2 control-label">Pesan</label>
          <div class="col-sm-10">
            {{ Form::textarea('pesan', null, ['class' => 'form-control', 'placeholder' => 'Pesan (jika ada)']) }}
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Konfirmasi</button>
          </div>
        </div>
      {{ Form::close() }}
      @endif
    </div>
  </div>
</div>
@stop
