@extends('frontend.layouts.master')

@section('css')

@stop

@section('top-js')

@stop

@section('js')

@stop

@section('content')
@include('frontend.user.sidebar')
<div class="col-lg-9">
  <div class="panel panel-default">
    <div class="panel-heading">{{ $desc }}</div>
    <div class="panel-body">
      <table class="table table-striped table-hover ">
        <thead>
          <tr>
            <th>ID</th>
            <th>Nama Produk</th>
            <th>Tanggal</th>
            <th>Total</th>
            <th>Status</th>
            <th width="20%"></th>
          </tr>
        </thead>
        <tbody>
          @foreach($order as $key => $value)
          <tr>
            <td>{{ $value->invoice->code }}</td>
            <td>{{ $value->product_name }}</td>
            <td>{{ $value->created_at }}</td>
            <td>{{ Helpers::rupiah($value->invoice->total) }}</td>
            <td>{{ strtoupper($value->status) }}</td>
            <td>
              <div class="btn-group">
                <a href="{{ URL::action('UserController@getOrderDetail', $value->invoice->code) }}" class="btn btn-info btn-xs">Detail</a>
                <a href="#" class="btn btn-success btn-xs">Pembayaran</a>
              </div>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@stop
