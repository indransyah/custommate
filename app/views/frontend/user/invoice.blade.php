@extends('frontend.layouts.master')

@section('css')

@stop

@section('top-js')

@stop

@section('js')

@stop

@section('content')
@include('frontend.user.sidebar')
<div class="col-lg-9">

  <div class="panel panel-default">
    <div class="panel-heading">{{ $desc }}</div>
    <div class="panel-body">
      <table class="table table-striped table-hover ">
        <thead>
          <tr>
            <th>#</th>
            <th>Tanggal</th>
            <th>Total</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>XXX1SD</td>
            <td>5 Desember 2015</td>
            <td>250000</td>
            <td>Verifikasi</td>
          </tr>
          <tr>
            <td>XXX1SD</td>
            <td>5 Desember 2015</td>
            <td>250000</td>
            <td>Verifikasi</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

</div>
@stop
