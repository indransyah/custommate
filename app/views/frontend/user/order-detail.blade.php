@extends('frontend.layouts.master')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('bower_components/ladda/dist/ladda-themeless.min.css') }}">
@stop

@section('top-js')

@stop

@section('js')
<script src="{{ asset('bower_components/ladda/dist/spin.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('bower_components/ladda/dist/ladda.min.js') }}" type="text/javascript"></script>
<script>
Ladda.bind( '.ladda-button' );
</script>
@stop

@section('content')
@include('frontend.user.sidebar')
<div class="col-lg-9">
  @if($invoice->order->status == 'upload')
  <div class="panel panel-warning">
    <div class="panel-heading">Unggah Vector</div>
    <div class="panel-body">
      {{ Form::open(array('action' => ['UserController@postVector', $invoice->code], 'class' => 'form-horizontal', 'files' => true)) }}
        <div class="form-group">
          <label for="destination" class="col-sm-2 control-label">Vector</label>
          <div class="col-sm-10">
            {{ Form::file('vector', ['required' => 'required']) }}
            {{ $errors->first('vector', '<p class="text-danger">:message</p>'); }}
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default ladda-button" data-color="green" data-style="slide-right"><span class="ladda-label">Unggah</span><span class="ladda-spinner"></span></button>
          </div>
        </div>
      {{ Form::close() }}
    </div>
  </div>
  @endif
  <div class="panel panel-default">
    <div class="panel-heading">{{ $desc }}</div>
    <div class="panel-body">
      <div class="row">
        <div class="col-md-6">
          <h3 class="text-center">Pesanan</h3>
          <dl class="dl-horizontal">
            <dt>ID Tagihan</dt>
            <dd>{{ $invoice->code }}</dd>
            <dt>Tanggal</dt>
            <dd>{{ $invoice->order->created_at }}</dd>
            <dt>Total Tagihan</dt>
            <dd>{{ Helpers::idr($invoice->total) }}</dd>
            <dt>Status Pesanan</dt>
            <dd><span class="label label-success">{{ strtoupper($invoice->order->status) }}</span></dd>
          </dl>
        </div>
        <div class="col-md-6">
          <h3 class="text-center">Pengiriman</h3>
          <dl class="dl-horizontal">
            <dt>Kurir</dt>
            <dd>{{ strtoupper($invoice->order->shipment->courier) }} ({{$invoice->order->shipment->service}})</dd>
            <dt>Ongkir</dt>
            <dd>{{ Helpers::idr($invoice->order->shipment->cost) }} ({{ $invoice->order->shipment->weight }} gram)</dd>
            <dt>Penerima</dt>
            <dd>{{ $invoice->order->shipment->recipient->name }} ({{ $invoice->order->shipment->recipient->phone }})</dd>
            <dt>Alamat</dt>
            <dd>{{ $invoice->order->shipment->recipient->address }} {{ $invoice->order->shipment->destination }} ({{ $invoice->order->shipment->recipient->postal_code }})</dd>
          </dl>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <h3>Product</h3>
          <p><strong>{{ $invoice->order->product_name }}</strong></p>
          <table class="table table-bordered">
            <tbody>
              <tr>
                <th class="col-md-6 text-center">Design</th>
                <th class="text-center">Price</th>
                <th class="text-center">Size</th>
                <th class="text-center">Qty</th>
                <th class="text-center">Subtotal</th>
              </tr>
              @foreach($invoice->order->product as $key => $product)
              <tr>
                <td rowspan="{{ $invoice->order->product->count() }}">
                  <img class="col-md-6 img-responsive" src="{{ URL::to('designs/'.$invoice->order->design->first()->front) }}">
                  <img class="col-md-6 img-responsive" src="{{ URL::to('designs/'.$invoice->order->design->first()->back) }}">
                </td>
                <td class="text-right">{{ Helpers::idr($product->pivot->price) }}</td>
                <td class="text-center">{{ $product->pivot->size }}</td>
                <td class="text-center">{{ $product->pivot->qty }}</td>
                <td class="text-right">{{ Helpers::idr($product->pivot->subtotal) }}</td>
              </tr>
              @endforeach
              <tr>
                <td colspan="4"><strong>Ongkir</strong></td>
                <td class="text-right">{{ Helpers::idr($invoice->order->shipment->cost) }}</td>
              </tr>
              <tr>
                <td colspan="4"><strong>Total</strong></td>
                <td class="text-right">{{ Helpers::idr($invoice->order->invoice->total) }}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
  </div>
</div>
@stop
