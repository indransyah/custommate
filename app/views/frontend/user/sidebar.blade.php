<div class="col-lg-3">
  <div class="list-group">
    <a href="{{ URL::action('UserController@getOrder') }}" class="list-group-item {{ Active::action(['UserController@getOrder', 'UserController@getOrderDetail']) }}">
      <strong>Pemesanan <i class="glyphicon glyphicon-chevron-right pull-right"></i></strong>
    </a>
    <!-- <a href="{{ URL::action('UserController@getInvoice') }}" class="list-group-item {{ Active::action('UserController@getInvoice') }}">
      <strong>Tagihan <i class="glyphicon glyphicon-chevron-right pull-right"></i></strong>
    </a> -->
    <a href="{{ URL::action('UserController@getPayment') }}" class="list-group-item {{ Active::action(['UserController@getPayment']) }}">
      <strong>Pembayaran <i class="glyphicon glyphicon-chevron-right pull-right"></i></strong>
    </a>
  </div>
</div>
