<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Custommate - Custom your apparel</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('bower_components/bootswatch/yeti/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custommate.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/sweetalert/dist/sweetalert.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('bower_components/bootstrap-social/bootstrap-social.css') }}" />
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Engagement">
    <link rel="stylesheet" href="{{ asset('bower_components/jquery-loading/dist/jquery.loading.css') }}" type="text/css">

    @yield('css')
    <script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('bower_components/sweetalert/dist/sweetalert.min.js') }}"></script>
    <script src="{{ asset('bower_components/jquery.fblogin/dist/jquery.fblogin.js') }}" type="text/javascript"></script>
    @yield('top-js')



    <!-- Custom styles for this template -->
    <style type="text/css">
    .page-header {
      margin: 20px 0px 20px;
    }

    .nav > li {
      display: inline-block;
    }
    .color-preview {
      border: 1px solid #CCC;
      margin: 2px;
      zoom: 1;
      vertical-align: top;
      cursor: pointer;
      overflow: hidden;
      width: 20px;
      height: 20px;
    }
    .navbar-static-top {
      margin-bottom: 19px;
    }
    .rotate {
      -webkit-transform:rotate(90deg);
      -moz-transform:rotate(90deg);
      -o-transform:rotate(90deg);
      /* filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=1.5); */
      -ms-transform:rotate(90deg);
    }
    /*.Arial{font-family:"Arial";}
      .Helvetica{font-family:"Helvetica";}
      .MyriadPro{font-family:"Myriad Pro";}
      .Delicious{font-family:"Delicious";}
      .Verdana{font-family:"Verdana";}
      .Georgia{font-family:"Georgia";}
      .Courier{font-family:"Courier";}
      .ComicSansMS{font-family:"Comic Sans MS";}
      .Impact{font-family:"Impact";}
      .Monaco{font-family:"Monaco";}
      .Optima{font-family:"Optima";}
      .HoeflerText{font-family:"Hoefler Text";}
      .Plaster{font-family:"Plaster";}
      .Engagement{font-family:"Engagement";}*/
    </style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <!-- Static navbar -->
    @include('frontend.layouts.header')

    <div class="container">

      <section id="typography">
        <div class="page-header">
          <h1>{{ $title }}</h1>
        </div>

        <div class="row">
          @include('frontend.layouts.alert')
          @yield('content')

        </div>

      </section>
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('bower_components/jquery-loading/dist/jquery.loading.js') }}"></script>
    @yield('js')
    <script>
    // $(document).ajaxSend(function(event, request, settings) {
    //   $('body').loading();
    // });
    //
    // $(document).ajaxComplete(function(event, request, settings) {
    //   $('body').loading('stop');
    // });

    $('#login').click(function() {
      swal({
        title: "Login!",
        text: '<p>Silakan login dengan akun Facebook Anda!</p><br><a class="btn btn-social btn-facebook"><i class="fa fa-facebook"></i>Sign in with Facebook</a>',
        html: true,
        showConfirmButton: false,
        allowOutsideClick: true
      });
    });

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN' : '{{ csrf_token() }}'
      }
    });

    window.fbAsyncInit = function() {
      FB.init({
        appId      : '1609127605993732',
        xfbml      : true,
        version    : 'v2.5'
      });
    };

    (function(d, s, id){
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {return;}
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    $(document).on('click', '.btn-facebook', function(e) {
      swal.close();
      $('body').loading();
      // the fields option is a comma separated list of valid FB fields
      $.fblogin({
        fbId: '1609127605993732',
        permissions: 'email',
        fields: 'name, email',
        success: function (data) {
          // data will only contain the requested fields plus the user FB id
          // which is always included by default
          // console.log(data);
          $.post("{{ URL::action('AuthController@postFacebook') }}", {
            id: data.id,
            name: data.name,
            email: data.email
          }).done(function($response) {
            location.reload();
          })
          .fail(function($response) {
            $('body').loading('stop');
            alert( "error" );
          });
        }
      });
    });
    $('#logout').click(function() {
      $('body').loading();
      $.getJSON("{{ URL::action('AuthController@getLogout') }}", function(response) {
        location.reload();
      });
    });
    </script>
  </body>
</html>
