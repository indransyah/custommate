<nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{ URL::action('HomeController@getIndex') }}">Custommate</a>
      <!-- <a class="logo" href="{{ URL::action('HomeController@getIndex') }}"><img src="{{ URL::to('img/logo.png') }}"></a> -->
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li class="active"><a href="{{ URL::action('ProductController@getIndex') }}">Product</a></li>
        <li><a href="#guide">FAQ</a></li>
        <li><a href="#about">FAQ</a></li>
        <li><a href="#contact">About</a></li>
        <li><a href="#contact">Contact Us</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <!-- <li><a href="#">Cart <span class="badge">{{ Cart::count() }}</span></a></li> -->
        <!-- <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Cart <span class="badge">{{ Cart::count() }}</span><span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Lihat Cart</a></li>
            <li><a href="#">Checkout</a></li>
            <li role="separator" class="divider"></li>
          </ul>
        </li> -->
        @if($status)
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ $customer->name }} <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="{{ URL::action('UserController@getOrder') }}">Akun</a></li>
            <li><a href="#" id="logout">Logout</a></li>
            <!-- <li role="separator" class="divider"></li> -->
          </ul>
        </li>
        @else
        <li><a href="#" id="login">Login</a></li>
        @endif
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</nav>
