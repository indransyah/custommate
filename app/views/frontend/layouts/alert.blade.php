@if (Session::has('error') || Session::has('success'))
<div class="col-lg-12">
  @if (Session::has('error'))
  <div class="alert alert-dismissible alert-danger">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {{ Session::get('error') }}
    {{ HTML::ul($errors->all()) }}
  </div>
  @elseif(Session::has('success'))
  <div class="alert alert-dismissible alert-success">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {{ Session::get('success') }}
  </div>
  @endif
</div>
@endif
