<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>{{ $title }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="{{ asset('bower_components/admin-lte/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="{{ asset('bower_components/components-font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset('bower_components/admin-lte/dist/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Loading -->
    <link rel="stylesheet" href="{{ asset('bower_components/jquery-loading/dist/jquery.loading.css') }}" type="text/css">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="{{ URL::to('/') }}"><b>Custom</b>mate</a>
      </div><!-- /.login-logo -->
      @include('backend.layouts.alert')

      <div class="login-box-body">
        <form action="" method="post">
          <div class="form-group has-feedback">
            <input disabled="true" type="email" class="form-control" placeholder="Email"/>
          </div>
          <div class="form-group has-feedback">
            <input disabled="true" type="password" class="form-control" placeholder="Password"/>
          </div>
          <div class="row">
            <div class="col-xs-4 pull-right">
              <button type="submit" class="btn btn-danger btn-block btn-flat disabled">Sign In</button>
            </div><!-- /.col -->
            <div class="col-xs-8 pull-left">
              <a style="text-decoration: line-through;">Tidak mempunyai akun, silakan daftar disini!</a>
            </div><!-- /.col -->
          </div>
        </form>
        <p class="login-box-msg" style="padding-top: 20px;padding-bottom: 0px;">Tidak perlu daftar, silakan masuk dengan akun Facebook Anda.</p>
        <div class="social-auth-links text-center">
          <a class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using Facebook</a>
        </div><!-- /.social-auth-links -->

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="{{ asset('bower_components/admin-lte/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset('bower_components/admin-lte/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <!-- Facebook login -->
    <script src="{{ asset('bower_components/jquery.fblogin/dist/jquery.fblogin.js') }}" type="text/javascript"></script>
    <!-- Loading -->
    <script src="{{ asset('bower_components/jquery-loading/dist/jquery.loading.js') }}"></script>
    <script>
    $(document).ajaxSend(function(event, request, settings) {
      $('body').loading();
    });

    $(document).ajaxComplete(function(event, request, settings) {
      $('body').loading('stop');
    });

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN' : '{{ csrf_token() }}'
      }
    });

    window.fbAsyncInit = function() {
      FB.init({
        appId      : '1609127605993732',
        xfbml      : true,
        version    : 'v2.5'
      });
    };

    (function(d, s, id){
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {return;}
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    $('.btn-facebook').click(function() {
      $('body').loading();
      // the fields option is a comma separated list of valid FB fields
      $.fblogin({
        fbId: '1609127605993732',
        permissions: 'email',
        fields: 'name, email',
        success: function (data) {
          // data will only contain the requested fields plus the user FB id
          // which is always included by default
          console.log(data);
          $.post("{{ URL::action('AuthController@postFacebook') }}", {
            id: data.id,
            name: data.name,
            email: data.email
          }).done(function($response) {
            window.location.assign("{{ URL::action('HomeController@getIndex') }}")
          })
          .fail(function($response) {
            alert( "error" );
          });
        }
      });
    });
    </script>
  </body>
</html>
