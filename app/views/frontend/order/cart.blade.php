@extends('frontend.layouts.master')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('bower_components/ladda/dist/ladda-themeless.min.css') }}">
@stop

@section('top-js')

@stop

@section('js')
<script src="{{ asset('bower_components/blueimp-file-upload/js/vendor/jquery.ui.widget.js') }}"></script>
<script src="{{ asset('bower_components/blueimp-file-upload/js/jquery.iframe-transport.js') }}"></script>
<script src="{{ asset('bower_components/blueimp-file-upload/js/jquery.fileupload.js') }}"></script>
<script src="{{ asset('bower_components/accounting/accounting.js') }}" type="text/javascript"></script>
<script src="{{ asset('bower_components/ladda/dist/spin.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('bower_components/ladda/dist/ladda.min.js') }}" type="text/javascript"></script>
<script>
$(document).ready(function () {
	Ladda.bind( '.ladda-button' );

	getCart();

	$('#add').click(function() {
		$('body').loading();
		var ukuran = $('select[name="ukuran"]').val();
		var jumlah = $('input[name="jumlah"]').val();
		$.post('{{ URL::action('OrderController@postAdd', $product->slug) }}', {
			ukuran: ukuran,
			jumlah: jumlah
		}).done(function(data) {
			getCart();
      swal({
          title: 'Sukses!',
          text: data.message,
          type: 'success',
          allowOutsideClick: true,
      });
		}).fail(function(data) {
			$('body').loading('stop');
			swal({
				type: 'error',
				title: 'Error!',
				text: data.responseJSON.message,
			});
		});
	});

	$(document).on('click', '#clear', function() {
    swal({
      title: "Konfirmasi",
      text: "Apakah Anda yakin akan menghapus semua pesanan?",
			type: 'warning',
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
			disableButtonsOnConfirm: true,
			confirmButtonColor: "#DD6B55",
    }, function(){
      $.post('{{ URL::action('OrderController@deleteClear') }}', {
  			_method: 'delete'
  		}).done(function(data) {
				getCart();
        swal({
            title: 'Sukses!',
            text: data.message,
            type: 'success',
            allowOutsideClick: true,
        });
  		}).fail(function(data) {
  			swal({
  				type: 'error',
  				title: 'Error!',
  				text: data.responseJSON.message,
  			});
  		});
    });
  });

	$(document).on('click', '.remove', function() {
    var rowid = $(this).data('rowid');
		swal({
			title: "Konfirmasi",
			text: "Apakah Anda ingin menghapus pesanan ini?",
			type: 'warning',
			showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
			disableButtonsOnConfirm: true,
			confirmButtonColor: "#DD6B55",
    },
    function(){
			$('body').loading();
      $.post('{{ URL::action('OrderController@deleteDelete') }}/'+rowid, {
  			_method: 'delete'
  		}).done(function(data) {
				getCart();
        swal({
            title: 'Sukses!',
            text: data.message,
            type: 'success',
            allowOutsideClick: true,
        });
  		}).fail(function(data) {
  			swal({
  				type: 'error',
  				title: 'Error!',
  				text: data.responseJSON.message,
  			});
  		});
    });
  });

  function getCart()
  {
		$('body').loading();
    $.getJSON('{{ URL::action('OrderController@getCartContent') }}', function(data) {
      $('#cart tbody').empty();
			if (data.length == 0) {
				$('#checkout').addClass('disabled');
				$('#cart tbody').append($('<tr>').append($('<td>').attr('colspan', '4').text('Kosong')));
			} else {
				$('#checkout').removeClass('disabled');
				var total = 0;
				$.each(data, function(key, value) {
					var tr = $('<tr>');
					$('#cart tbody').append(tr);
					total += value.subtotal;
					$('<td>').text(value.options.size).appendTo(tr);
					$('<td>').text(value.qty).appendTo(tr);
					$('<td>').attr('class', 'subtotal').text(accounting.formatMoney(value.subtotal, "Rp ", 2, ".", ",")).appendTo(tr);
					$('<td>').append($('<button>').attr({
						'class' : 'btn btn-danger btn-xs remove',
						'data-rowid' : value.rowid,
						'title': 'Hapus pesanan'
					}).append($('<i>').attr('class', 'glyphicon glyphicon-remove'))).appendTo(tr);
				});
				var tr = $('<tr>');
				$('#cart tbody').append(tr);
				$('<td>').attr('colspan', '2').text('Total').appendTo(tr);
				$('<td>').append($('<strong>').text(accounting.formatMoney(total, "Rp ", 2, ".", ","))).appendTo(tr);
				$('<td>').append($('<button>').attr({
					'id': 'clear',
					'class': 'btn btn-danger btn-xs',
					'title': 'Hapus semua pesanan'
				}).append($('<i>').attr('class', 'glyphicon glyphicon-trash'))).appendTo(tr);
			}
			$('body').loading('stop');
    });
  }
})
</script>
@stop

@section('content')
<div class="col-md-8">
  <div class="panel panel-default">
    <div class="panel-heading">Preview</div>
    <div class="panel-body">
      <div class="col-md-6">
        <img class="img-responsive" src="{{ URL::to('designs/temp/'.Session::get('design.front')) }}">
      </div>
      <div class="col-md-6">
        <img class="img-responsive" src="{{ URL::to('designs/temp/'.Session::get('design.back')) }}">
      </div>
    </div>
  </div>
</div>
<div class="col-md-4">
  <div class="panel panel-default">
    <div class="panel-heading">Cart</div>
    <div class="panel-body">
			<strong>{{ $product->name }} {{ '@'.Helpers::rupiah($product->price) }}</strong>
      <table id="cart" class="table table-striped table-hover text-center">
        <thead>
          <tr>
            <th width="25%">Ukuran</th>
            <th width="25%">Jumlah</th>
            <th width="40%">Subtotal</th>
            <th width="10%"></th>
          </tr>
        </thead>
				<tfoot>
					<tr>
							<td>
								<select class="form-control input-sm" name="ukuran">
									<option>S</option>
									<option>M</option>
									<option>L</option>
									<option>XL</option>
								</select>
							</td>
							<td>
								<input type="number" class="form-control input-sm" name="jumlah" value="1">
							</td>
							<td>
								<button id="add" class="btn btn-block btn-large btn-info btn-sm" type="submit" title="Tambahkan ke cart"><i class="glyphicon glyphicon-plus"></i></button>
							</td>
							<td></td>
          </tr>
				</tfoot>
        <tbody>
        </tbody>
      </table>
      <a href="{{ URL::action('OrderController@getCheckout') }}" class="ladda-button btn btn-block btn-large btn-success{{ (Cart::count()>0) ? '' : ' disabled' }}" name="checkout" id="checkout" data-color="green" data-style="slide-right">Checkout <i class="glyphicon glyphicon-shopping-cart"></i></a>
    </div>
  </div>
</div>
@stop
