@extends('frontend.layouts.master')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('bower_components/ladda/dist/ladda-themeless.min.css') }}">
@stop

@section('top-js')

@stop

@section('js')
<script src="{{ asset('bower_components/accounting/accounting.js') }}" type="text/javascript"></script>
<script src="{{ asset('bower_components/ladda/dist/spin.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('bower_components/ladda/dist/ladda.min.js') }}" type="text/javascript"></script>
<script>
Ladda.bind( '.ladda-button' );
var weight = {{ $product->weight }}
$(document).ready(function(){
  accounting.settings = {
    currency: {
      symbol : "Rp",   // default currency symbol is '$'
      format: "%s %v", // controls output: %s = symbol, %v = value/number (can be object: see below)
      decimal : ",",  // decimal point separator
      thousand: ".",  // thousands separator
      precision : 2   // decimal places
    },
    number: {
      precision : 0,  // default precision on numbers is 0
      thousand: ".",
      decimal : ","
    }
  }

  $('body').loading();

  // Mengambil data provinsi
  $.getJSON('{{ URL::action('RajaOngkirController@getProvince') }}', function(result) {
    $.each(result, function(key, value){
      $('#provinsi').append($('<option></option>').attr('value', value.province_id).text(value.province));
    });
    @if(!is_null(Input::old('provinsi')))
    $('#provinsi').val({{Input::old('provinsi')}});
    @endif
  }).fail(function() {
    $('#kabupaten').empty();
      swal({
      title: "Error!",
      text: "Tidak dapat menggambil data. Refresh halaman?",
      type: "error",
      closeOnConfirm: false
    },
    function(){
      window.location.assign('{{ URL::action("OrderController@getCheckout") }}')
    });
  }).done(function() {
    // jika berhasil kemudian mengambil data kabupaten
    getCity($('#provinsi').val());
  });

  $('#provinsi').change(function() {
    $('body').loading();
    resetOngkirAndTotal();
    getCity(this.value);
  });

  $('#kabupaten').change(function() {
    $('body').loading();
    resetOngkirAndTotal();
    getCost(this.value, $('#kurir').val());
  });

  $('#kurir').change(function() {
    $('body').loading();
    resetOngkirAndTotal();
    getCost($('#kabupaten').val(), this.value);
  });

  $('#servis').change(function() {
    addOngkirAndTotal($(this).children('option:selected').data('cost'));
  });

  function addOngkirAndTotal(service_value)
  {
    var ongkir = parseInt(service_value);
    var total = ongkir + parseInt($('#total').data('total'));
    $('#ongkir').text(accounting.formatMoney(ongkir));
    $('#total').text(accounting.formatMoney(total));
    $('#baris-ongkir').attr('class', 'info');
    $('#baris-total').attr('class', 'success');
  }

  function resetOngkirAndTotal()
  {
    $('#ongkir').text('Rp 0');
    var total = $('#total');
    total.text(accounting.formatMoney(total.data('total')));
    $('#baris-ongkir').attr('class', '');
    $('#baris-total').attr('class', '');
  }

  function getCity(province_id)
  {
    $.getJSON('{{ URL::action('RajaOngkirController@getCity') }}/'+province_id, function(result){
      $('#kabupaten').empty();
      $.each(result, function(key, value){
        $('#kabupaten').append($('<option></option>').attr('value', value.city_id).text(value.city_name));
      });
      @if(!is_null(Input::old('provinsi')))
      $('#kabupaten').val({{Input::old('kabupaten')}});
      @endif
    }).fail(function() {
      $('#kabupaten').empty();
      alert("Tidak dapat menggambil data.");
    }).done(function() {
      getCost($('#kabupaten').val(), $('#kurir').val());
    });
  }

  function getCost(destination_city_id, courier)
  {
    $.post('{{ URL::action('RajaOngkirController@postCost') }}', {
      destination: destination_city_id,
      weight: weight,
      courier: courier
    }).done(function(result) {
      $('#servis').empty();
      $.each(result[0].costs, function(key, value){
        $('#servis').append($('<option></option>').attr('value', value.service).data('cost', value.cost[0].value).text(value.service + ' ('+value.description+') ' + accounting.formatMoney(value.cost[0].value)));
      });
      if(result[0].costs.length != 0) {
        $('#servis-error').empty();
        addOngkirAndTotal($('#servis option:selected').data('cost'));
      } else {
        $('#servis-error').append('<p class="text-danger">Layanan tidak didukung di kota Anda, silakan pilih kurir lain!</p>')
      }
      $('body').loading('stop');
    });
  }
});
</script>
@stop

@section('content')
<div class="col-md-6">
  <div class="panel panel-default">
    <div class="panel-heading">Pesanan</div>
    <div class="panel-body">
      <legend>{{ $product->name }}</legend>
      <table class="table table-striped table-hover">
        <thead>
          <tr>
            <th width="30%">Ukuran</th>
            <th width="30%">Jumlah</th>
            <th width="40%">Subtotal</th>
          </tr>
        </thead>
        <tbody>
          @foreach($cart as $value)
          <tr>
            <td>{{ $value->options->size }}</td>
            <td>{{ $value->qty }}</td>
            <td>{{ Helpers::rupiah($value->subtotal) }}</td>
          </tr>
          @endforeach
          <tr id="baris-ongkir">
            <td colspan="2"><strong>Ongkos Kirim</strong></td>
            <td><strong><span id="ongkir">Rp 0</span></strong></td>
          </tr>
          <tr id="baris-total">
            <td colspan="2"><strong>Total</strong></td>
            <td><strong><span id="total" data-total="{{ Cart::total() }}">{{ Helpers::rupiah(Cart::total()) }}</span></strong></td>
          </tr>
        </tbody>
      </table>
      <div class="col-md-6">
        <img class="img-responsive" src="{{ URL::to('designs/temp/'.Session::get('design.front')) }}">
      </div>
      <div class="col-md-6">
        <img class="img-responsive" src="{{ URL::to('designs/temp/'.Session::get('design.back')) }}">
      </div>
    </div>
  </div>
</div>
<div class="col-md-6">
  <div class="panel panel-default">
    <div class="panel-heading">Pengiriman</div>
    <div class="panel-body">
      {{ Form::open(array('action' => 'OrderController@postCheckout', 'files' => true, 'class' => 'form-horizontal')) }}
        <fieldset>
          <legend>Identitas Penerima</legend>
          <div class="form-group {{ ($errors->has('nama')) ? 'has-error' : '' }}">
            <label for="nama" class="col-lg-2 control-label">Nama</label>
            <div class="col-lg-10">
              {{ Form::text('nama', null, ['id' => 'nama', 'class' => 'form-control', 'placeholder' => 'Nama Lengkap', 'required' => 'required']) }}
              {{ $errors->first('nama', '<p class="text-danger">:message</p>'); }}
            </div>
          </div>
          <div class="form-group {{ ($errors->has('telepon')) ? 'has-error' : '' }}">
            <label for="telepon" class="col-lg-2 control-label">Telepon</label>
            <div class="col-lg-10">
              {{ Form::text('telepon', null, ['id' => 'telepon', 'class' => 'form-control', 'placeholder' => 'Nomor Telepon, misal : 085729123456', 'required' => 'required']) }}
              {{ $errors->first('telepon', '<p class="text-danger">:message</p>'); }}
            </div>
          </div>
          <legend>Alamat Penerima</legend>
          <div class="form-group {{ ($errors->has('provinsi')) ? 'has-error' : '' }}">
            <label for="provinsi" class="col-lg-2 control-label">Provinsi</label>
            <div class="col-lg-10">
              <select class="form-control" id="provinsi" name="provinsi" required="required">
              </select>
              {{ $errors->first('provinsi', '<p class="text-danger">:message</p>'); }}
            </div>
          </div>
          <div class="form-group {{ ($errors->has('kabupaten')) ? 'has-error' : '' }}">
            <label for="kabupaten" class="col-lg-2 control-label">Kabupaten</label>
            <div class="col-lg-10">
              <select class="form-control" id="kabupaten" name="kabupaten" required="required">
              </select>
              {{ $errors->first('kabupaten', '<p class="text-danger">:message</p>'); }}
            </div>
          </div>
          <div class="form-group {{ ($errors->has('alamat')) ? 'has-error' : '' }}">
            <label for="telepon" class="col-lg-2 control-label">Alamat</label>
            <div class="col-lg-10">
              {{ Form::textarea('alamat', null, ['id' => 'alamat', 'class' => 'form-control', 'rows' => '3', 'placeholder' => 'Alamat Lengkap', 'required' => 'required']) }}
              {{ $errors->first('alamat', '<p class="text-danger">:message</p>'); }}
            </div>
          </div>
          <div class="form-group {{ ($errors->has('kode_pos')) ? 'has-error' : '' }}">
            <label for="nama" class="col-lg-2 control-label">Kode Pos</label>
            <div class="col-lg-10">
              {{ Form::text('kode_pos', null, ['id' => 'kode_pos', 'class' => 'form-control', 'placeholder' => 'Kode Pos', 'required' => 'required']) }}
              {{ $errors->first('kode_pos', '<p class="text-danger">:message</p>'); }}
            </div>
          </div>
          <div class="form-group {{ ($errors->has('kurir')) ? 'has-error' : '' }}">
            <label for="kurir" class="col-lg-2 control-label">Kurir</label>
            <div class="col-lg-10">
              {{ Form::select('kurir', array('jne' => 'JNE', 'tiki' => 'Tiki', 'pos' => 'POS'), null, array('class' => 'form-control', 'id' => 'kurir', 'required' => 'reqquired')) }}
              {{ $errors->first('kurir', '<p class="text-danger">:message</p>'); }}
            </div>
          </div>
          <div class="form-group {{ ($errors->has('servis')) ? 'has-error' : '' }}">  <!-- removed id="servis-form" style="display: none;" -->
            <label class="col-lg-2 control-label">Servis</label>
            <div class="col-lg-10">
              <select class="form-control" id="servis" name="servis" required="required">
              </select>
              <div id="servis-error">
              {{ $errors->first('servis', '<p class="text-danger">:message</p>'); }}
              </div>
            </div>
          </div>
          <legend>Desain</legend>
          <div class="form-group {{ ($errors->has('design')) ? 'has-error' : '' }}">
            <label for="desain" class="col-lg-2 control-label">Desain</label>
            <div class="col-lg-10">
              {{ Form::file('desain') }}
              {{ $errors->first('desain', '<p class="text-danger">:message</p>'); }}
            </div>
          </div>
          <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
              <button type="submit" class="btn btn-large btn-success ladda-button" data-color="green" data-style="slide-right"><span class="ladda-label">Submit</span><span class="ladda-spinner"></span></button>
            </div>
          </div>
        </fieldset>
      {{ Form::close() }}
    </div>
  </div>
</div>
@stop
