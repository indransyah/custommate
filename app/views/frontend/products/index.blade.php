@extends('frontend.layouts.master')

@section('css')
<style media="screen">
  .product {
    margin-bottom: 50px;
  }
  .label-inverse {
    font-size: 18px;
    background-color: #000;
  }
</style>
@stop

@section('top-js')
@stop

@section('js')
@stop

@section('content')
@foreach($products as $product)
<div class="col-md-3 text-center product">
  <a href="{{ URL::action('ProductController@getShow', $product->slug) }}">
    <img src="{{ $product->front->url() }}" class="img-responsive">
  </a>
  <p><strong>{{ $product->name }}</strong></p>
  <p class="label label-inverse">{{ Helpers::rupiah($product->price) }}</p>
</div>
@endforeach
@stop
