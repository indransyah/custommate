<?php

return array(
	'table' => 'oauth_identities',
	'providers' => array(
		'facebook' => array(
			'id' => '1609127605993732',
			'secret' => '814eacc4d9aba4b27e7fe7953daf065e',
			'redirect' => URL::to('facebook/login'),
			'scope' => array('email'),
		),
		'google' => array(
			'id' => '12345678',
			'secret' => 'y0ur53cr374ppk3y',
			'redirect' => URL::to('your/google/redirect'),
			'scope' => array(),
		),
		'github' => array(
			'id' => '12345678',
			'secret' => 'y0ur53cr374ppk3y',
			'redirect' => URL::to('your/github/redirect'),
			'scope' => array(),
		),
		'linkedin' => array(
			'id' => '12345678',
			'secret' => 'y0ur53cr374ppk3y',
			'redirect' => URL::to('your/linkedin/redirect'),
			'scope' => array(),
		),
		'instagram' => array(
			'id' => '12345678',
			'secret' => 'y0ur53cr374ppk3y',
			'redirect' => URL::to('your/instagram/redirect'),
			'scope' => array(),
		),
	)
);
