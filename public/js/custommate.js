var canvas;
var tshirts = new Array(); //prototype: [{style:'x',color:'white',front:'a',back:'b',price:{tshirt:'12.95',frontPrint:'4.99',backPrint:'4.99',total:'22.47'}}]
var a;
var b;
var line1;
var line2;
var line3;
var line4;

// Background image for canvas
var imgFront = document.createElement("IMG");
imgFront.src = frontImageUrl;
var imgBack = document.createElement("IMG");
imgBack.src = backImageUrl;

// Canvas for front design
var x =  document.createElement("CANVAS");
x.width = 200;
x.height = 400;
var xContext = x.getContext("2d");

// Canvas for back design
var y =  document.createElement("CANVAS");
y.width = 200;
y.height = 400;
var yContext = y.getContext("2d");

$(document).ready(function() {
  //setup front side canvas
  canvas = new fabric.Canvas('tcanvas', {
    hoverCursor: 'pointer',
    selection: true,
    selectionBorderColor:'blue'
  });
  canvas.on({
    'object:moving': function(e) {
      e.target.opacity = 0.5;
    },
    'object:modified': function(e) {
      e.target.opacity = 1;
    },
    'object:selected':onObjectSelected,
    'selection:cleared':onSelectedCleared
  });
  // piggyback on `canvas.findTarget`, to fire "object:over" and "object:out" events
  canvas.findTarget = (function(originalFn) {
    return function() {
      var target = originalFn.apply(this, arguments);
      if (target) {
        if (this._hoveredTarget !== target) {
          canvas.fire('object:over', { target: target });
          if (this._hoveredTarget) {
            canvas.fire('object:out', { target: this._hoveredTarget });
          }
          this._hoveredTarget = target;
        }
      }
      else if (this._hoveredTarget) {
        canvas.fire('object:out', { target: this._hoveredTarget });
        this._hoveredTarget = null;
      }
      return target;
    };
  })(canvas.findTarget);

  canvas.on('object:over', function(e) {
    //e.target.setFill('red');
    //canvas.renderAll();
  });

  canvas.on('object:out', function(e) {
    //e.target.setFill('green');
    //canvas.renderAll();
  });

  $("#text-string").keyup(function(){
    var activeObject = canvas.getActiveObject();
    if (activeObject && activeObject.type === 'text') {
      activeObject.text = this.value;
      canvas.renderAll();
    }
  });
  $(document).on('click', '.img-polaroid', function(e) {
    var el = e.target;
    /*temp code*/
    var offset = 50;
    var left = fabric.util.getRandomInt(0 + offset, 200 - offset);
    var top = fabric.util.getRandomInt(0 + offset, 400 - offset);
    var angle = fabric.util.getRandomInt(-20, 40);
    var width = fabric.util.getRandomInt(30, 50);
    var opacity = (function(min, max){ return Math.random() * (max - min) + min; })(0.5, 1);

    fabric.Image.fromURL(el.src, function(image) {
      image.set({
        left: left,
        top: top,
        angle: 0,
        padding: 10,
        cornersize: 10,
        hasRotatingPoint:true
      });
      //image.scale(getRandomNum(0.1, 0.25)).setCoords();
      canvas.add(image);
    });
  });
  document.getElementById('remove-selected').onclick = function() {
    var activeObject = canvas.getActiveObject(),
    activeGroup = canvas.getActiveGroup();
    if (activeObject) {
      canvas.remove(activeObject);
      $("#text-string").val("");
    }
    else if (activeGroup) {
      var objectsInGroup = activeGroup.getObjects();
      canvas.discardActiveGroup();
      objectsInGroup.forEach(function(object) {
        canvas.remove(object);
      });
    }
  };
  document.getElementById('bring-to-front').onclick = function() {
    var activeObject = canvas.getActiveObject(),
    activeGroup = canvas.getActiveGroup();
    if (activeObject) {
      activeObject.bringToFront();
    }
    else if (activeGroup) {
      var objectsInGroup = activeGroup.getObjects();
      canvas.discardActiveGroup();
      objectsInGroup.forEach(function(object) {
        object.bringToFront();
      });
    }
  };
  document.getElementById('send-to-back').onclick = function() {
    var activeObject = canvas.getActiveObject(),
    activeGroup = canvas.getActiveGroup();
    if (activeObject) {
      activeObject.sendToBack();
    }
    else if (activeGroup) {
      var objectsInGroup = activeGroup.getObjects();
      canvas.discardActiveGroup();
      objectsInGroup.forEach(function(object) {
        object.sendToBack();
      });
    }
  };

  //canvas.add(new fabric.fabric.Object({hasBorders:true,hasControls:false,hasRotatingPoint:false,selectable:false,type:'rect'}));
  $("#drawingArea").hover(
    function() {
      canvas.add(line1);
      canvas.add(line2);
      canvas.add(line3);
      canvas.add(line4);
      canvas.renderAll();
    },
    function() {
      canvas.remove(line1);
      canvas.remove(line2);
      canvas.remove(line3);
      canvas.remove(line4);
      canvas.renderAll();
    }
  );

  $('.color-preview').click(function(){
    var color = $(this).css("background-color");
    document.getElementById("shirtDiv").style.backgroundColor = color;
  });

  $('#flip').click( function() {
    onSelectedCleared();
    if (document.getElementById("tshirtFacing").alt == "front") {
      $("#tshirtFacing").attr("src", backImageUrl);
      $("#tshirtFacing").attr("alt","back");
      a = JSON.stringify(canvas);
      // canvas.deactivateAll().renderAll();
      xContext.clearRect(0, 0, x.width, x.height);
      xContext.drawImage(canvas.getElement(), 0, 0);
      canvas.clear();
      try {
      //  var json = JSON.parse(b);
       canvas.loadFromJSON(b, canvas.renderAll.bind(canvas));
      } catch(e) {

      }
    } else {
      $("#tshirtFacing").attr("src", frontImageUrl);
      $("#tshirtFacing").attr("alt","front");
      b = JSON.stringify(canvas);
      // canvas.deactivateAll().renderAll();
      yContext.clearRect(0, 0, y.width, y.height);
      yContext.drawImage(canvas.getElement(), 0, 0);
      canvas.clear();
      try {
        // var json = JSON.parse(a);
        canvas.loadFromJSON(a, canvas.renderAll.bind(canvas));
      } catch(e) {

      }
    }
    // canvas.renderAll();
    // setTimeout(function() {
    //   canvas.calcOffset();
    // },200);
  });

  $('#clear').click(function() {
    canvas.clear();
    xContext.clearRect(0, 0, x.width, x.height);
    yContext.clearRect(0, 0, y.width, y.height);
    a = null;
    b = null;
  });

  $('#order').click(function() {
    $('body').loading();
    var front = drawDesign('front'); // another function => var front = drawFront();
    var frontDataURL = front.toDataURL('image/png');
    // window.open(frontDataURL);
    var back = drawDesign('back'); // another function => var back = drawBack();
    var backDataURL = back.toDataURL('image/png');
    // window.open(backDataURL);
    $.ajax({
      method: "POST",
      url: postDesignUrl,
      data: {
        'front': frontDataURL,
        'back': backDataURL
      },
      success: function (d) {
         window.location.assign(redirectUrl)
      },
      error: function (e) {
        Ladda.stopAll();
        $('body').loading('stop');
        swal({
          type: "error",
          title: "Error!",
          text: e.responseJSON.message,
          confirmButtonText: "Ok"
        });
      }
    });
  });

  $('#fileupload').fileupload({
    url: postImageUrl,
    // acceptFileTypes: '/(\.|\/)(gif|jpe?g|png)$/i',
    dataType: 'json',
    add: function(e, data) {
      $('body').loading();
      var uploadErrors = [];
      var acceptFileTypes = /(\.|\/)(gif|jpe?g|png)$/i;
      if(data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
        uploadErrors.push('Not an accepted file type');
      }
      if(data.originalFiles[0]['size'].length && data.originalFiles[0]['size'] > 5000) {
        uploadErrors.push('Filesize is too big');
      }
      if(uploadErrors.length > 0) {
        swal({
          type: "error",
          title: "Error!",
          text: uploadErrors.join("\n"),
          confirmButtonText: "Ok"
        });
      } else {
        data.submit();
      }
    },
    done: function (e, data) {
      $.each(data.result.files, function (index, file) {
        $('<img/>').attr('src', file.url).attr('class', 'img-polaroid img-responsive').attr('style', 'cursor:pointer;').appendTo('#avatarlist');
      });
      $('body').loading('stop');
    },
    // progressall: function (e, data) {
    //   var progress = parseInt(data.loaded / data.total * 100, 10);
    //   $('#progress .progress-bar').css('width', progress + '%');
    // },
    fail: function(e, data) {
      $('body').loading('stop');
      swal({
        type: "error",
        title: "Error!",
        text: data.jqXHR.responseJSON.message.image.join("\n"),
        confirmButtonText: "Ok"
      });
    }
  }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');

  $(".clearfix button,a").tooltip();
  line1 = new fabric.Line([0,0,200,0], {"stroke":"#000000", "strokeWidth":1,hasBorders:false,hasControls:false,hasRotatingPoint:false,selectable:false});
  line2 = new fabric.Line([199,0,200,399], {"stroke":"#000000", "strokeWidth":1,hasBorders:false,hasControls:false,hasRotatingPoint:false,selectable:false});
  line3 = new fabric.Line([0,0,0,400], {"stroke":"#000000", "strokeWidth":1,hasBorders:false,hasControls:false,hasRotatingPoint:false,selectable:false});
  line4 = new fabric.Line([0,400,200,399], {"stroke":"#000000", "strokeWidth":1,hasBorders:false,hasControls:false,hasRotatingPoint:false,selectable:false});
});//doc ready

function getRandomNum(min, max) {
  return Math.random() * (max - min) + min;
}
function onObjectSelected(e) {
  var selectedObject = e.target;
  selectedObject.hasRotatingPoint = true
  if (selectedObject && selectedObject.type === 'image'){
    //display image editor
    $("#imageeditor").css('display', 'block');
  }
}
function onSelectedCleared(e){
  $("#imageeditor").css('display', 'none');
}
function removeWhite(){
  var activeObject = canvas.getActiveObject();
  if (activeObject && activeObject.type === 'image') {
    activeObject.filters[2] =  new fabric.Image.filters.RemoveWhite({hreshold: 100, distance: 10});//0-255, 0-255
    activeObject.applyFilters(canvas.renderAll.bind(canvas));
  }
}
function drawDesign(position) {
  var tempCanvas;
  var imageDesign;
  onSelectedCleared();
  canvas.deactivateAll().renderAll();
  tempCanvas = canvas.getElement();
  if (position == 'front') {
    if (document.getElementById("tshirtFacing").alt == 'back') {
      tempCanvas = x;
    }
    imageDesign = imgFront;
  } else if (position == 'back') {
    if (document.getElementById("tshirtFacing").alt == 'front') {
      tempCanvas = y;
    }
    imageDesign = imgBack;
  }
  var design = document.createElement("CANVAS");
  design.width = 530;
  design.height = 630;
  var ctx = design.getContext("2d");
  ctx.fillStyle = document.getElementById("shirtDiv").style.backgroundColor;
  ctx.fillRect(0, 0, design.width, design.height);
  ctx.drawImage(tempCanvas, 160, 100);
  ctx.drawImage(imageDesign, 0, 0);
  return design;
}
function drawBack() {
  var backCanvas;
  if (document.getElementById("tshirtFacing").alt == "back") {
    onSelectedCleared();
    canvas.deactivateAll().renderAll();
    backCanvas = canvas.getElement();
  } else {
    backCanvas = y;
  }
  var back = document.createElement("CANVAS");
  back.width = 530;
  back.height = 630;
  var ctx = back.getContext("2d");
  ctx.fillStyle = document.getElementById("shirtDiv").style.backgroundColor;
  ctx.fillRect(0, 0, back.width, back.height);
  ctx.drawImage(backCanvas, 160, 100);
  // var backImage = document.getElementById("tshirtBack");
  // var img = document.createElement("IMG");
  // img.src = 'http://custommate.app/img/crew_back.png';
  ctx.drawImage(imgBack, 0, 0);
  return back;
}
function drawFront() {
  var frontCanvas;
  if (document.getElementById("tshirtFacing").alt == "front") {
    // frontCanvas = canvas;
    onSelectedCleared();
    canvas.deactivateAll().renderAll();
    frontCanvas = canvas.getElement();
  } else {
    frontCanvas = x;
  }
  var front = document.createElement("CANVAS");
  front.width = 530;
  front.height = 630;
  var ctx = front.getContext("2d");
  ctx.fillStyle = document.getElementById("shirtDiv").style.backgroundColor;
  ctx.fillRect(0, 0, front.width, front.height);
  ctx.drawImage(frontCanvas, 160, 100);
  // var img = document.createElement("IMG");
  // img.src = 'http://custommate.app/img/crew_front.png';
  // var img = document.getElementById("tshirtFront");
  ctx.drawImage(imgFront, 0, 0);
  return front;
}
